<?php

return [
    'ACCUEIL_ETU'  => [
        'libelle'                  => 'Accueil des étudiants',
        'taux-remu'                => 'TAUX_1',
        'taux-remu-majore'         => null,
        'accompagnement-etudiants' => false,
    ],
    'ACCOMP_HANDI' => [
        'libelle'                  => 'Assistance et accompagnement d’étudiants handicapés',
        'taux-remu'                => 'TAUX_1',
        'taux-remu-majore'         => null,
        'accompagnement-etudiants' => true,
    ],
    'TUTORAT'      => [
        'libelle'                  => 'Tutorat',
        'taux-remu'                => 'TAUX_1',
        'taux-remu-majore'         => null,
        'accompagnement-etudiants' => false,
    ],
    'SOUTIEN_TIC'  => [
        'libelle'                  => 'Soutien informatique et aide à l’utilisation des nouvelles technologies',
        'taux-remu'                => 'TAUX_1',
        'taux-remu-majore'         => null,
        'accompagnement-etudiants' => false,
    ],
    'APPUI_BIBLIO' => [
        'libelle'                  => 'Service d’appui aux personnels des bibliothèques',
        'taux-remu'                => 'TAUX_1',
        'taux-remu-majore'         => null,
        'accompagnement-etudiants' => false,
    ],
    'ANIM_CSSS'    => [
        'libelle'                  => 'Animations culturelles, scientifiques, sportives et sociales',
        'taux-remu'                => 'TAUX_1',
        'taux-remu-majore'         => null,
        'accompagnement-etudiants' => false,
    ],
    'AIDE_INS_PRO' => [
        'libelle'                  => 'Aide à l’insertion professionnelle',
        'taux-remu'                => 'TAUX_1',
        'taux-remu-majore'         => null,
        'accompagnement-etudiants' => false,
    ],
    'PROMO_ODF'    => [
        'libelle'                  => 'Promotion de l’offre de formation',
        'taux-remu'                => 'TAUX_1',
        'taux-remu-majore'         => null,
        'accompagnement-etudiants' => false,
    ],
];