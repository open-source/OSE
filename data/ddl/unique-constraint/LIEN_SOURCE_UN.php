<?php

//@formatter:off

return [
    'name'    => 'LIEN_SOURCE_UN',
    'table'   => 'LIEN',
    'index'   => 'LIEN_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
