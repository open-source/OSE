<?php

//@formatter:off

return [
    'name'    => 'TBL_PLA_INT_UN',
    'table'   => 'TBL_PLAFOND_INTERVENANT',
    'index'   => 'TBL_PLA_INT_UN',
    'columns' => [
        'INTERVENANT_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'ANNEE_ID',
        'PLAFOND_ID',
    ],
];

//@formatter:on
