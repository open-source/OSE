<?php

//@formatter:off

return [
    'name'    => 'STATUT_UN',
    'table'   => 'STATUT',
    'index'   => 'STATUT_UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
        'ANNEE_ID',
    ],
];

//@formatter:on
