<?php

//@formatter:off

return [
    'name'    => 'TBL_PIECE_JOINTE_FOURNIE_UN',
    'table'   => 'TBL_PIECE_JOINTE_FOURNIE',
    'index'   => 'TBL_PIECE_JOINTE_FOURNIE_UN',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
        'INTERVENANT_ID',
        'VALIDATION_ID',
        'FICHIER_ID',
    ],
];

//@formatter:on
