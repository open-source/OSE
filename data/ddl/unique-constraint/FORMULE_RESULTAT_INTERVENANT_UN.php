<?php

//@formatter:off

return [
    'name'    => 'FORMULE_RESULTAT_INTERVENANT_UN',
    'table'   => 'FORMULE_RESULTAT_INTERVENANT',
    'index'   => 'FORMULE_RESULTAT_INTERVENANT_UN',
    'columns' => [
        'INTERVENANT_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'ETAT_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
