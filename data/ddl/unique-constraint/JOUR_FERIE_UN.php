<?php

//@formatter:off

return [
    'name'    => 'JOUR_FERIE_UN',
    'table'   => 'JOUR_FERIE',
    'index'   => 'JOUR_FERIE_UN',
    'columns' => [
        'DATE_JOUR',
    ],
];

//@formatter:on
