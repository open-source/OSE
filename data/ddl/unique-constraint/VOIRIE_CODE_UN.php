<?php

//@formatter:off

return [
    'name'    => 'VOIRIE_CODE_UN',
    'table'   => 'VOIRIE',
    'index'   => 'VOIRIE_CODE_UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
