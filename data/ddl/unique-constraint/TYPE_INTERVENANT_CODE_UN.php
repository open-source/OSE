<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENANT_CODE_UN',
    'table'   => 'TYPE_INTERVENANT',
    'index'   => 'TYPE_INTERVENANT_CODE_UN',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
