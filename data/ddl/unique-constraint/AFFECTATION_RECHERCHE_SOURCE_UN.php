<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_RECHERCHE_SOURCE_UN',
    'table'   => 'AFFECTATION_RECHERCHE',
    'index'   => 'AFFECTATION_RECHERCHE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
