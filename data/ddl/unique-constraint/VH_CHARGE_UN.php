<?php

//@formatter:off

return [
    'name'    => 'VH_CHARGE_UN',
    'table'   => 'VOLUME_HORAIRE_CHARGE',
    'index'   => 'VH_CHARGE_UN',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'TYPE_INTERVENTION_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
