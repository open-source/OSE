<?php

//@formatter:off

return [
    'name'    => 'HISTO_INTERVENANT_SERVICE__UN',
    'table'   => 'HISTO_INTERVENANT_SERVICE',
    'index'   => 'HISTO_INTERVENANT_SERVICE__UN',
    'columns' => [
        'INTERVENANT_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'REFERENTIEL',
    ],
];

//@formatter:on
