<?php

//@formatter:off

return [
    'name'    => 'ETAT_SORTIE_CODE_UN',
    'table'   => 'ETAT_SORTIE',
    'index'   => 'ETAT_SORTIE_CODE_UN',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
