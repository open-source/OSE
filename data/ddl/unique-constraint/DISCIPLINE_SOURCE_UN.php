<?php

//@formatter:off

return [
    'name'    => 'DISCIPLINE_SOURCE_UN',
    'table'   => 'DISCIPLINE',
    'index'   => 'DISCIPLINE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
