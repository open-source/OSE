<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_CODE_UN',
    'table'   => 'INTERVENANT',
    'index'   => 'INTERVENANT_CODE_UN',
    'columns' => [
        'CODE',
        'ANNEE_ID',
        'STATUT_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
