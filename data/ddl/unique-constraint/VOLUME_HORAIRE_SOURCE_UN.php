<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_SOURCE_UN',
    'table'   => 'VOLUME_HORAIRE',
    'index'   => 'VOLUME_HORAIRE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
