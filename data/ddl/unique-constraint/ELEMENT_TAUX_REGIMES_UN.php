<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_TAUX_REGIMES_UN',
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'index'   => 'ELEMENT_TAUX_REGIMES_UN',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
