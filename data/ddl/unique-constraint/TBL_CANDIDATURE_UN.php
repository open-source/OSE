<?php

//@formatter:off

return [
    'name'    => 'TBL_CANDIDATURE_UN',
    'table'   => 'TBL_CANDIDATURE',
    'index'   => 'TBL_CANDIDATURE_UN',
    'columns' => [
        'ANNEE_ID',
        'INTERVENANT_ID',
        'OFFRE_EMPLOI_ID',
    ],
];

//@formatter:on
