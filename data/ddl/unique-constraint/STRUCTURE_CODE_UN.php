<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_CODE_UN',
    'table'   => 'STRUCTURE',
    'index'   => 'STRUCTURE_CODE_UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
