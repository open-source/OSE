<?php

//@formatter:off

return [
    'name'    => 'CHEMIN_PEDAGOGIQUE__UN',
    'table'   => 'CHEMIN_PEDAGOGIQUE',
    'index'   => 'CHEMIN_PEDAGOGIQUE__UN',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'ETAPE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
