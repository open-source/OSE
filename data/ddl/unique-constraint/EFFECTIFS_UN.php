<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_UN',
    'table'   => 'EFFECTIFS',
    'index'   => 'EFFECTIFS_UN',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
