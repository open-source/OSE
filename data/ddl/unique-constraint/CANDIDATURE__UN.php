<?php

//@formatter:off

return [
    'name'    => 'CANDIDATURE__UN',
    'table'   => 'CANDIDATURE',
    'index'   => 'CANDIDATURE__UN',
    'columns' => [
        'INTERVENANT_ID',
        'OFFRE_EMPLOI_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
