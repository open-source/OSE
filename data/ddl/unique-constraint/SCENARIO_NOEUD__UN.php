<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_NOEUD__UN',
    'table'   => 'SCENARIO_NOEUD',
    'index'   => 'SCENARIO_NOEUD__UN',
    'columns' => [
        'SCENARIO_ID',
        'NOEUD_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
