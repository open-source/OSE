<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_SOURCE_UN',
    'table'   => 'TYPE_INTERVENTION',
    'index'   => 'TYPE_INTERVENTION_SOURCE_UN',
    'columns' => [
        'HISTO_DESTRUCTION',
        'SOURCE_CODE',
    ],
];

//@formatter:on
