<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_SOURCE_UN',
    'table'   => 'EMPLOYEUR',
    'index'   => 'EMPLOYEUR_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
