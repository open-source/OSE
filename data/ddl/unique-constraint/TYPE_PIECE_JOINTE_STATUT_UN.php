<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_STATUT_UN',
    'table'   => 'TYPE_PIECE_JOINTE_STATUT',
    'index'   => 'TYPE_PIECE_JOINTE_STATUT_UN',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
        'STATUT_ID',
        'HISTO_DESTRUCTION',
        'NUM_REGLE',
    ],
];

//@formatter:on
