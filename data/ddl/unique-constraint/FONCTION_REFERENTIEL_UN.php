<?php

//@formatter:off

return [
    'name'    => 'FONCTION_REFERENTIEL_UN',
    'table'   => 'FONCTION_REFERENTIEL',
    'index'   => 'FONCTION_REFERENTIEL_UN',
    'columns' => [
        'ANNEE_ID',
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
