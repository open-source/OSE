<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_MISSION_SOURCE_UN',
    'table'   => 'VOLUME_HORAIRE_MISSION',
    'index'   => 'VOLUME_HORAIRE_MISSION_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
