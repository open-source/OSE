<?php

//@formatter:off

return [
    'name'    => 'TYPE_AGREMENT__UN',
    'table'   => 'TYPE_AGREMENT',
    'index'   => 'TYPE_AGREMENT__UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
