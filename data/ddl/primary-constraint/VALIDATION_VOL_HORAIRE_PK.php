<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_VOL_HORAIRE_PK',
    'table'   => 'VALIDATION_VOL_HORAIRE',
    'index'   => 'VALIDATION_VOL_HORAIRE_PK',
    'columns' => [
        'VALIDATION_ID',
        'VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
