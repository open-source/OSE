<?php

//@formatter:off

return [
    'name'    => 'DOSSIER_CHAMP_AUTRE_PK',
    'table'   => 'DOSSIER_CHAMP_AUTRE',
    'index'   => 'DOSSIER_CHAMP_AUTRE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
