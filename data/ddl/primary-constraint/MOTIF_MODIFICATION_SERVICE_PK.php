<?php

//@formatter:off

return [
    'name'    => 'MOTIF_MODIFICATION_SERVICE_PK',
    'table'   => 'MOTIF_MODIFICATION_SERVICE',
    'index'   => 'MOTIF_MODIFICATION_SERVICE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
