<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_PK',
    'table'   => 'TYPE_PIECE_JOINTE',
    'index'   => 'TYPE_PIECE_JOINTE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
