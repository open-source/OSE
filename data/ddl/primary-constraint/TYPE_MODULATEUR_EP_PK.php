<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_EP_PK',
    'table'   => 'TYPE_MODULATEUR_EP',
    'index'   => 'TYPE_MODULATEUR_EP_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
