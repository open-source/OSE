<?php

//@formatter:off

return [
    'name'    => 'PERIMETRE_PK',
    'table'   => 'PERIMETRE',
    'index'   => 'PERIMETRE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
