<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENANT_PK',
    'table'   => 'TYPE_INTERVENANT',
    'index'   => 'TYPE_INTERVENANT_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
