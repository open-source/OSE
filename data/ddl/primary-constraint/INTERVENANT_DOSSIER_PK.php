<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_DOSSIER_PK',
    'table'   => 'INTERVENANT_DOSSIER',
    'index'   => 'INTERVENANT_DOSSIER_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
