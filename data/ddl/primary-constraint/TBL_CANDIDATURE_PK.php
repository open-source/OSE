<?php

//@formatter:off

return [
    'name'    => 'TBL_CANDIDATURE_PK',
    'table'   => 'TBL_CANDIDATURE',
    'index'   => 'TBL_CANDIDATURE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
