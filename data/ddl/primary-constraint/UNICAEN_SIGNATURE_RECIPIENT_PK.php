<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_RECIPIENT_PK',
    'table'   => 'UNICAEN_SIGNATURE_RECIPIENT',
    'index'   => 'UNICAEN_SIGNATURE_RECIPIENT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
