<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_PK',
    'table'   => 'TBL_SERVICE',
    'index'   => 'TBL_SERVICE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
