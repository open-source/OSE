<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_PK',
    'table'   => 'VALIDATION',
    'index'   => 'VALIDATION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
