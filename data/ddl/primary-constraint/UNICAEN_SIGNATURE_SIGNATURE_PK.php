<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_SIGNATURE_PK',
    'table'   => 'UNICAEN_SIGNATURE_SIGNATURE',
    'index'   => 'UNICAEN_SIGNATURE_SIGNATURE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
