<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_SIGNATUREFLOW_PK',
    'table'   => 'UNICAEN_SIGNATURE_SIGNATUREFLOW',
    'index'   => 'UNICAEN_SIGNATURE_SIGNATUREFLOW',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
