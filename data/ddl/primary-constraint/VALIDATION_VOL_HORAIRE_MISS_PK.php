<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_VOL_HORAIRE_MISS_PK',
    'table'   => 'VALIDATION_VOL_HORAIRE_MISS',
    'index'   => 'VALIDATION_VOL_HORAIRE_MISS_PK',
    'columns' => [
        'VALIDATION_ID',
        'VOLUME_HORAIRE_MISSION_ID',
    ],
];

//@formatter:on
