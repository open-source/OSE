<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_PK',
    'table'   => 'INTERVENANT',
    'index'   => 'INTERVENANT_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
