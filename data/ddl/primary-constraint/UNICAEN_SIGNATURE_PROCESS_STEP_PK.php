<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_PROCESS_STEP_PK',
    'table'   => 'UNICAEN_SIGNATURE_PROCESS_STEP',
    'index'   => 'UNICAEN_SIGNATURE_PROCESS_STEP',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
