<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_OBSERVER_PK',
    'table'   => 'UNICAEN_SIGNATURE_OBSERVER',
    'index'   => 'UNICAEN_SIGNATURE_OBSERVER',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
