<?php

//@formatter:off

return [
    'name'    => 'MISSION_PK',
    'table'   => 'MISSION',
    'index'   => 'MISSION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
