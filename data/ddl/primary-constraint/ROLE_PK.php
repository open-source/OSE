<?php

//@formatter:off

return [
    'name'    => 'ROLE_PK',
    'table'   => 'ROLE',
    'index'   => 'ROLE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
