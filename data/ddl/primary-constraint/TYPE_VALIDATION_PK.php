<?php

//@formatter:off

return [
    'name'    => 'TYPE_VALIDATION_PK',
    'table'   => 'TYPE_VALIDATION',
    'index'   => 'TYPE_VALIDATION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
