<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_PROCESS_PK',
    'table'   => 'UNICAEN_SIGNATURE_PROCESS',
    'index'   => 'UNICAEN_SIGNATURE_PROCESS',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
