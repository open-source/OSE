<?php

//@formatter:off

return [
    'name'    => 'TAUX_REMU_VALEUR_PK',
    'table'   => 'TAUX_REMU_VALEUR',
    'index'   => 'TAUX_REMU_VALEUR_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
