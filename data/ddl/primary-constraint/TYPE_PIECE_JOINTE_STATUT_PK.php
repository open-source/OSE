<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_STATUT_PK',
    'table'   => 'TYPE_PIECE_JOINTE_STATUT',
    'index'   => 'TYPE_PIECE_JOINTE_STATUT_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
