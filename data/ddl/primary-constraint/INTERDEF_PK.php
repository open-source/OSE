<?php

//@formatter:off

return [
    'name'    => 'INTERDEF_PK',
    'table'   => 'INTERVENANT_PAR_DEFAUT',
    'index'   => 'INTERDEF_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
