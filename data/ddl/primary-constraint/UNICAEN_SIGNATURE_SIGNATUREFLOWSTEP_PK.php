<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_SIGNATUREFLOWSTEP_PK',
    'table'   => 'UNICAEN_SIGNATURE_SIGNATUREFLOWSTEP',
    'index'   => 'UNICAEN_SIGNATURE_SIGNATUREFLOWSTEP',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
