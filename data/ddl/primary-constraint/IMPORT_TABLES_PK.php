<?php

//@formatter:off

return [
    'name'    => 'IMPORT_TABLES_PK',
    'table'   => 'IMPORT_TABLES',
    'index'   => 'IMPORT_TABLES_PK',
    'columns' => [
        'TABLE_NAME',
    ],
];

//@formatter:on
