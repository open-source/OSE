<?php

//@formatter:off

return [
    'name'    => 'HISTO_SERVICE_MODIFICATION_PK',
    'table'   => 'HISTO_INTERVENANT_SERVICE',
    'index'   => 'HISTO_SERVICE_MODIFICATION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
