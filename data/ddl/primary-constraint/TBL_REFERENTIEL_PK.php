<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_PK',
    'table'   => 'TBL_REFERENTIEL',
    'index'   => 'TBL_REFERENTIEL_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
