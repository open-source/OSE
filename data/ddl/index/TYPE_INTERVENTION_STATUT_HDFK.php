<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_STATUT_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STATUT',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
