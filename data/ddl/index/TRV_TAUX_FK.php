<?php

//@formatter:off

return [
    'name'    => 'TRV_TAUX_FK',
    'unique'  => FALSE,
    'table'   => 'TAUX_REMU_VALEUR',
    'columns' => [
        'TAUX_REMU_ID',
    ],
];

//@formatter:on
