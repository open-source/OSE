<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'STRUCTURE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
