<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STATUT_HMFK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STATUT',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
