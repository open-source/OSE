<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_EP_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_EP',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
