<?php

//@formatter:off

return [
    'name'    => 'VHR_TYPE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_REF',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
