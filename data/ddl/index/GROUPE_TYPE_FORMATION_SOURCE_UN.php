<?php

//@formatter:off

return [
    'name'    => 'GROUPE_TYPE_FORMATION_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'GROUPE_TYPE_FORMATION',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
