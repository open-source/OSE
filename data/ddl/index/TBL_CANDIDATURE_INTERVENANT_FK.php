<?php

//@formatter:off

return [
    'name'    => 'TBL_CANDIDATURE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CANDIDATURE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
