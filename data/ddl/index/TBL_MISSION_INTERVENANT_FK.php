<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_MISSION',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
