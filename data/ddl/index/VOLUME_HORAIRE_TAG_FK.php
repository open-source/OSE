<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_TAG_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'TAG_ID',
    ],
];

//@formatter:on
