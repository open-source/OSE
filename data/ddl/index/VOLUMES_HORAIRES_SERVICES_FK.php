<?php

//@formatter:off

return [
    'name'    => 'VOLUMES_HORAIRES_SERVICES_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'SERVICE_ID',
    ],
];

//@formatter:on
