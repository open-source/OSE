<?php

//@formatter:off

return [
    'name'    => 'FORMULE_R_INT_TYPE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_INTERVENANT',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
