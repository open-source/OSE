<?php

//@formatter:off

return [
    'name'    => 'TYPE_RESSOURCE_FI',
    'unique'  => FALSE,
    'table'   => 'TYPE_RESSOURCE',
    'columns' => [
        'FI',
    ],
];

//@formatter:on
