<?php

//@formatter:off

return [
    'name'    => 'TBL_DOSSIER_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_DOSSIER',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
