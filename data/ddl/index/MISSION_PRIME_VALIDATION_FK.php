<?php

//@formatter:off

return [
    'name'    => 'MISSION_PRIME_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'MISSION_PRIME',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
