<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_DOSSIER_SITUATION_MATRIMONIALE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'SITUATION_MATRIMONIALE_ID',
    ],
];

//@formatter:on
