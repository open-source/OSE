<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_SERVICE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'SERVICE_ID',
    ],
];

//@formatter:on
