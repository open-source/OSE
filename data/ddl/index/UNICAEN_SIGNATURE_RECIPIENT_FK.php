<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_RECIPIENT_FK',
    'unique'  => FALSE,
    'table'   => 'UNICAEN_SIGNATURE_RECIPIENT',
    'columns' => [
        'SIGNATURE_ID',
    ],
];

//@formatter:on
