<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_PROCESS_STEP_PFK',
    'unique'  => FALSE,
    'table'   => 'UNICAEN_SIGNATURE_PROCESS_STEP',
    'columns' => [
        'PROCESS_ID',
    ],
];

//@formatter:on
