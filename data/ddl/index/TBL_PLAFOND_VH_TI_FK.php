<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_VH_TI_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_VOLUME_HORAIRE',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
