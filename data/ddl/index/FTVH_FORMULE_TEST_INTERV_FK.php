<?php

//@formatter:off

return [
    'name'    => 'FTVH_FORMULE_TEST_INTERV_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_TEST_VOLUME_HORAIRE',
    'columns' => [
        'FORMULE_INTERVENANT_TEST_ID',
    ],
];

//@formatter:on
