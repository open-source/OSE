<?php

//@formatter:off

return [
    'name'    => 'ROLE_HCFK',
    'unique'  => FALSE,
    'table'   => 'ROLE',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
