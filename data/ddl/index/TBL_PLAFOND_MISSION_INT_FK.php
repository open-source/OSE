<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_MISSION_INT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_MISSION',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
