<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_RECHERCHE',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'CRITERE_RECHERCHE',
    ],
];

//@formatter:on
