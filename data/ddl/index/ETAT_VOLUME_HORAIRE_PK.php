<?php

//@formatter:off

return [
    'name'    => 'ETAT_VOLUME_HORAIRE_PK',
    'unique'  => TRUE,
    'table'   => 'ETAT_VOLUME_HORAIRE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
