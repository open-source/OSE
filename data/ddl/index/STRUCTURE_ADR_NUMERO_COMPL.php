<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_ADR_NUMERO_COMPL',
    'unique'  => FALSE,
    'table'   => 'STRUCTURE',
    'columns' => [
        'ADRESSE_NUMERO_COMPL_ID',
    ],
];

//@formatter:on
