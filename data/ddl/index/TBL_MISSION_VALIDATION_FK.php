<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_MISSION',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
