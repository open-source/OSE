<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_ELEMENT_EP_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_ELEMENT',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
