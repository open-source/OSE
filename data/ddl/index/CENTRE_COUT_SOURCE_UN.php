<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'CENTRE_COUT',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
