<?php

//@formatter:off

return [
    'name'    => 'TR_LIB_UN',
    'unique'  => TRUE,
    'table'   => 'TAUX_REMU',
    'columns' => [
        'LIBELLE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
