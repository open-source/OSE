<?php

//@formatter:off

return [
    'name'    => 'FONCTION_REFERENTIEL_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'FONCTION_REFERENTIEL',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
