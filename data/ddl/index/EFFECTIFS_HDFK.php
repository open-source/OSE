<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_HDFK',
    'unique'  => FALSE,
    'table'   => 'EFFECTIFS',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
