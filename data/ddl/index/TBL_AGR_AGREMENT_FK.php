<?php

//@formatter:off

return [
    'name'    => 'TBL_AGR_AGREMENT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_AGREMENT',
    'columns' => [
        'AGREMENT_ID',
    ],
];

//@formatter:on
