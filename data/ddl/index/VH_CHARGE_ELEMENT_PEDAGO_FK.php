<?php

//@formatter:off

return [
    'name'    => 'VH_CHARGE_ELEMENT_PEDAGO_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_CHARGE',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
