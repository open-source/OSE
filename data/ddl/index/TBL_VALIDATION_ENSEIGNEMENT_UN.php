<?php

//@formatter:off

return [
    'name'    => 'TBL_VALIDATION_ENSEIGNEMENT_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_VALIDATION_ENSEIGNEMENT',
    'columns' => [
        'INTERVENANT_ID',
        'STRUCTURE_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'SERVICE_ID',
        'VOLUME_HORAIRE_ID',
        'VALIDATION_ID',
    ],
];

//@formatter:on
