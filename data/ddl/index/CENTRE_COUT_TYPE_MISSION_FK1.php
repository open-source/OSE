<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_TYPE_MISSION_FK1',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_TYPE_MISSION',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
