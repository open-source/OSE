<?php

//@formatter:off

return [
    'name'    => 'TBL_CANDIDATURE_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CANDIDATURE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
