<?php

//@formatter:off

return [
    'name'    => 'MISSION_PRIME_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'MISSION_PRIME',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
