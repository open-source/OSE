<?php

//@formatter:off

return [
    'name'    => 'SERVICE_ETABLISSEMENT_FK',
    'unique'  => FALSE,
    'table'   => 'SERVICE',
    'columns' => [
        'ETABLISSEMENT_ID',
    ],
];

//@formatter:on
