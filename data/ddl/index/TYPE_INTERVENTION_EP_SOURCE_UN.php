<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_EP_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_INTERVENTION_EP',
    'columns' => [
        'HISTO_DESTRUCTION',
        'SOURCE_CODE',
    ],
];

//@formatter:on
