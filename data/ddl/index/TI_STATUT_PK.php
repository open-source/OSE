<?php

//@formatter:off

return [
    'name'    => 'TI_STATUT_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_INTERVENTION_STATUT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
