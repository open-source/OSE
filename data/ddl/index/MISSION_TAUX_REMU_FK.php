<?php

//@formatter:off

return [
    'name'    => 'MISSION_TAUX_REMU_FK',
    'unique'  => FALSE,
    'table'   => 'MISSION',
    'columns' => [
        'TAUX_REMU_ID',
    ],
];

//@formatter:on
