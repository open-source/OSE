<?php

//@formatter:off

return [
    'name'    => 'TYPE_MISSION_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MISSION',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
