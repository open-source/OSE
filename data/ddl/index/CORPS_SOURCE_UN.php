<?php

//@formatter:off

return [
    'name'    => 'CORPS_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'CORPS',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
