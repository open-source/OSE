<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_ADR_VOIRIE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'ADRESSE_VOIRIE_ID',
    ],
];

//@formatter:on
