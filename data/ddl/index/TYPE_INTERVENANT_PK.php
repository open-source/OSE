<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENANT_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_INTERVENANT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
