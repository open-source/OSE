<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_SERVICE_REFERENTIEL_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'SERVICE_REFERENTIEL_ID',
    ],
];

//@formatter:on
