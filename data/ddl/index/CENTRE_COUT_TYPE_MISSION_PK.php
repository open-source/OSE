<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_TYPE_MISSION_PK',
    'unique'  => TRUE,
    'table'   => 'CENTRE_COUT_TYPE_MISSION',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
