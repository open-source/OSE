<?php

//@formatter:off

return [
    'name'    => 'TVE_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_ENSEIGNEMENT',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
