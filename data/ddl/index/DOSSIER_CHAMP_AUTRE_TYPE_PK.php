<?php

//@formatter:off

return [
    'name'    => 'DOSSIER_CHAMP_AUTRE_TYPE_PK',
    'unique'  => TRUE,
    'table'   => 'DOSSIER_CHAMP_AUTRE_TYPE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
