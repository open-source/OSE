<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_PROCESS',
    'unique'  => TRUE,
    'table'   => 'UNICAEN_SIGNATURE_PROCESS',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
