<?php

//@formatter:off

return [
    'name'    => 'FORMULE_R_INT_TYPE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_INTERVENANT',
    'columns' => [
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
