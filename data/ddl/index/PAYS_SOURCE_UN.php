<?php

//@formatter:off

return [
    'name'    => 'PAYS_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'PAYS',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
