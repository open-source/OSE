<?php

//@formatter:off

return [
    'name'    => 'TBL_CLO_REAL_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CLOTURE_REALISE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
