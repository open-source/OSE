<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'CONTRAT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
