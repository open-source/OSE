<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_ETAPE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'ETAPE_ID',
    ],
];

//@formatter:on
