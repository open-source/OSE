<?php

//@formatter:off

return [
    'name'    => 'LIEN_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'LIEN',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
