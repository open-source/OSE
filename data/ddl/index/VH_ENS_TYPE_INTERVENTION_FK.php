<?php

//@formatter:off

return [
    'name'    => 'VH_ENS_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_ENS',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
