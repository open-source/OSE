<?php

//@formatter:off

return [
    'name'    => 'SERVICE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'SERVICE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
