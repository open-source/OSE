<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_UK',
    'unique'  => TRUE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'CONTRAT_ID',
        'VOLUME_HORAIRE_ID',
        'CONTRAT_PARENT_ID',
        'VOLUME_HORAIRE_REF_ID',
        'VOLUME_HORAIRE_MISSION_ID',
        'DATE_FIN',
    ],
];

//@formatter:on
