<?php

//@formatter:off

return [
    'name'    => 'TVR_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_REFERENTIEL',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
