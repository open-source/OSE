<?php

//@formatter:off

return [
    'name'    => 'TBL_CSD_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS_SEUILS_DEF',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
