<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_PROCESS_FK',
    'unique'  => FALSE,
    'table'   => 'UNICAEN_SIGNATURE_PROCESS',
    'columns' => [
        'SIGNATUREFLOW_ID',
    ],
];

//@formatter:on
