<?php

//@formatter:off

return [
    'name'    => 'TIS_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STRUCTURE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
