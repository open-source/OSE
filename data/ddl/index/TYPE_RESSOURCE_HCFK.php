<?php

//@formatter:off

return [
    'name'    => 'TYPE_RESSOURCE_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_RESSOURCE',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
