<?php

//@formatter:off

return [
    'name'    => 'TBL_CANDIDATURE_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CANDIDATURE',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
