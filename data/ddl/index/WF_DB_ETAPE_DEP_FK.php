<?php

//@formatter:off

return [
    'name'    => 'WF_DB_ETAPE_DEP_FK',
    'unique'  => FALSE,
    'table'   => 'WF_DEP_BLOQUANTE',
    'columns' => [
        'WF_ETAPE_DEP_ID',
    ],
];

//@formatter:on
