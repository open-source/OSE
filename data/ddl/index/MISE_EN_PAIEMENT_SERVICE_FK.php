<?php

//@formatter:off

return [
    'name'    => 'MISE_EN_PAIEMENT_SERVICE_FK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'SERVICE_ID',
    ],
];

//@formatter:on
