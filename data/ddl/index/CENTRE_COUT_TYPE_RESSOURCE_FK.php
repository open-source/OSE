<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_TYPE_RESSOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT',
    'columns' => [
        'TYPE_RESSOURCE_ID',
    ],
];

//@formatter:on
