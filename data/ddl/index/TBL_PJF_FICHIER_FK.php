<?php

//@formatter:off

return [
    'name'    => 'TBL_PJF_FICHIER_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE_FOURNIE',
    'columns' => [
        'FICHIER_ID',
    ],
];

//@formatter:on
