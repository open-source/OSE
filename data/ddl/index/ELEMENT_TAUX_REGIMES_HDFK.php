<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_TAUX_REGIMES_HDFK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
