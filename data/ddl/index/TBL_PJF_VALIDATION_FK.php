<?php

//@formatter:off

return [
    'name'    => 'TBL_PJF_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE_FOURNIE',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
