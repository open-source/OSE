<?php

//@formatter:off

return [
    'name'    => 'TR_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'TAUX_REMU',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
