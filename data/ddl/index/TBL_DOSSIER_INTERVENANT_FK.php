<?php

//@formatter:off

return [
    'name'    => 'TBL_DOSSIER_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_DOSSIER',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
