<?php

//@formatter:off

return [
    'name'    => 'ROLE_HDFK',
    'unique'  => FALSE,
    'table'   => 'ROLE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
