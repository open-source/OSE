<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_R_HMFK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION_RECHERCHE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
