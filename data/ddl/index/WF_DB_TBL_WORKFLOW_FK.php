<?php

//@formatter:off

return [
    'name'    => 'WF_DB_TBL_WORKFLOW_FK',
    'unique'  => FALSE,
    'table'   => 'WF_DEP_BLOQUANTE',
    'columns' => [
        'TBL_WORKFLOW_ID',
    ],
];

//@formatter:on
