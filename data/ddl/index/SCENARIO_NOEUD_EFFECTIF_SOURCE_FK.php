<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_NOEUD_EFFECTIF_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_NOEUD_EFFECTIF',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
