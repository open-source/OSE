<?php

//@formatter:off

return [
    'name'    => 'FORMULE_RESULTAT_INTERVENANT_UN',
    'unique'  => TRUE,
    'table'   => 'FORMULE_RESULTAT_INTERVENANT',
    'columns' => [
        'INTERVENANT_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'ETAT_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
