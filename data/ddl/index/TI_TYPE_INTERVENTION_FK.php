<?php

//@formatter:off

return [
    'name'    => 'TI_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION',
    'columns' => [
        'TYPE_INTERVENTION_MAQUETTE_ID',
    ],
];

//@formatter:on
