<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
