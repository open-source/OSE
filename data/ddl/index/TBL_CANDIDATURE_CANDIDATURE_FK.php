<?php

//@formatter:off

return [
    'name'    => 'TBL_CANDIDATURE_CANDIDATURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CANDIDATURE',
    'columns' => [
        'CANDIDATURE_ID',
    ],
];

//@formatter:on
