<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_PROCESS_STEP',
    'unique'  => TRUE,
    'table'   => 'UNICAEN_SIGNATURE_PROCESS_STEP',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
