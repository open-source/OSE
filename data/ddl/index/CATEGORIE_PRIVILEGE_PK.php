<?php

//@formatter:off

return [
    'name'    => 'CATEGORIE_PRIVILEGE_PK',
    'unique'  => TRUE,
    'table'   => 'CATEGORIE_PRIVILEGE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
