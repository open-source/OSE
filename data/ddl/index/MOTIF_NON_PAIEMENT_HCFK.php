<?php

//@formatter:off

return [
    'name'    => 'MOTIF_NON_PAIEMENT_HCFK',
    'unique'  => FALSE,
    'table'   => 'MOTIF_NON_PAIEMENT',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
