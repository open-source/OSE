<?php

//@formatter:off

return [
    'name'    => 'GRADE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'GRADE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
