<?php

//@formatter:off

return [
    'name'    => 'TAG_HMFK',
    'unique'  => FALSE,
    'table'   => 'TAG',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
