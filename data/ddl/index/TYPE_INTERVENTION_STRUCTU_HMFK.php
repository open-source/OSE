<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_STRUCTU_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STRUCTURE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
