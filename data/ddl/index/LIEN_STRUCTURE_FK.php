<?php

//@formatter:off

return [
    'name'    => 'LIEN_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'LIEN',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
