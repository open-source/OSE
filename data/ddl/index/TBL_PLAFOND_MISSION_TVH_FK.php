<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_MISSION_TVH_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_MISSION',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
