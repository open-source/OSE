<?php

//@formatter:off

return [
    'name'    => 'ETAPE_TYPE_FORMATION_FK',
    'unique'  => FALSE,
    'table'   => 'ETAPE',
    'columns' => [
        'TYPE_FORMATION_ID',
    ],
];

//@formatter:on
