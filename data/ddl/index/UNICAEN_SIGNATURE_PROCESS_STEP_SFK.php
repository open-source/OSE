<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_PROCESS_STEP_SFK',
    'unique'  => TRUE,
    'table'   => 'UNICAEN_SIGNATURE_PROCESS_STEP',
    'columns' => [
        'SIGNATURE_ID',
    ],
];

//@formatter:on
