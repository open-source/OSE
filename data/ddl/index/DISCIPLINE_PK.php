<?php

//@formatter:off

return [
    'name'    => 'DISCIPLINE_PK',
    'unique'  => TRUE,
    'table'   => 'DISCIPLINE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
