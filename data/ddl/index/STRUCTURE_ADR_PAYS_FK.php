<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_ADR_PAYS_FK',
    'unique'  => FALSE,
    'table'   => 'STRUCTURE',
    'columns' => [
        'ADRESSE_PAYS_ID',
    ],
];

//@formatter:on
