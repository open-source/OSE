<?php

//@formatter:off

return [
    'name'    => 'TYPE_HEURES_TYPE_HEURES_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_HEURES',
    'columns' => [
        'TYPE_HEURES_ELEMENT_ID',
    ],
];

//@formatter:on
