<?php

//@formatter:off

return [
    'name'    => 'SERVICE_REFERENTIEL_HCFK',
    'unique'  => FALSE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
