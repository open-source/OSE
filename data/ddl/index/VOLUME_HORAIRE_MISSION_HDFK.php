<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_MISSION_HDFK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_MISSION',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
