<?php

//@formatter:off

return [
    'name'    => 'EP_TAUX_REMU_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'TAUX_REMU_ID',
    ],
];

//@formatter:on
