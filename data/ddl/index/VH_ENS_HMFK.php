<?php

//@formatter:off

return [
    'name'    => 'VH_ENS_HMFK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_ENS',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
