<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_MISSION_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'VOLUME_HORAIRE_MISSION',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
