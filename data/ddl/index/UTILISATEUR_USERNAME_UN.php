<?php

//@formatter:off

return [
    'name'    => 'UTILISATEUR_USERNAME_UN',
    'unique'  => TRUE,
    'table'   => 'UTILISATEUR',
    'columns' => [
        'USERNAME',
    ],
];

//@formatter:on
