<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_ELEMENT_FK',
    'unique'  => FALSE,
    'table'   => 'EFFECTIFS',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
