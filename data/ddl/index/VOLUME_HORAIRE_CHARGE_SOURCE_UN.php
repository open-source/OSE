<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_CHARGE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'VOLUME_HORAIRE_CHARGE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
