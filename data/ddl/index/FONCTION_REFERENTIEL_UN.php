<?php

//@formatter:off

return [
    'name'    => 'FONCTION_REFERENTIEL_UN',
    'unique'  => TRUE,
    'table'   => 'FONCTION_REFERENTIEL',
    'columns' => [
        'CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
