<?php

//@formatter:off

return [
    'name'    => 'VHM_TYPE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_MISSION',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
