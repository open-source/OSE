<?php

//@formatter:off

return [
    'name'    => 'CC_ACTIVITE_FC',
    'unique'  => FALSE,
    'table'   => 'CC_ACTIVITE',
    'columns' => [
        'FC',
    ],
];

//@formatter:on
