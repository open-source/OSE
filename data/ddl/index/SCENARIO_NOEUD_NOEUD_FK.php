<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_NOEUD_NOEUD_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_NOEUD',
    'columns' => [
        'NOEUD_ID',
    ],
];

//@formatter:on
