<?php

//@formatter:off

return [
    'name'    => 'ROLE_PRIVILEGE_PK',
    'unique'  => TRUE,
    'table'   => 'ROLE_PRIVILEGE',
    'columns' => [
        'PRIVILEGE_ID',
        'ROLE_ID',
    ],
];

//@formatter:on
