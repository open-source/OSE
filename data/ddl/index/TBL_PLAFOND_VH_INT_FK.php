<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_VH_INT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_VOLUME_HORAIRE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
