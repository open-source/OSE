<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_HDFK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
