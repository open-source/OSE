<?php

//@formatter:off

return [
    'name'    => 'CHEMIN_PEDAGOGIQUE_HDFK',
    'unique'  => FALSE,
    'table'   => 'CHEMIN_PEDAGOGIQUE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
