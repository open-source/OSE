<?php

//@formatter:off

return [
    'name'    => 'SEUIL_CHARGE_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'SEUIL_CHARGE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
