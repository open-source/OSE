<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_PEDAGOGIQUE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
