<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_TVH_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
