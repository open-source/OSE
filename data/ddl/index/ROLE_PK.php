<?php

//@formatter:off

return [
    'name'    => 'ROLE_PK',
    'unique'  => TRUE,
    'table'   => 'ROLE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
