<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_MISSION_HMFK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_MISSION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
