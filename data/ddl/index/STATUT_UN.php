<?php

//@formatter:off

return [
    'name'    => 'STATUT_UN',
    'unique'  => TRUE,
    'table'   => 'STATUT',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
        'ANNEE_ID',
    ],
];

//@formatter:on
