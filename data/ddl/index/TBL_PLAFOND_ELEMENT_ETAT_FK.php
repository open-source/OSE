<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_ELEMENT_ETAT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_ELEMENT',
    'columns' => [
        'PLAFOND_ETAT_ID',
    ],
];

//@formatter:on
