<?php

//@formatter:off

return [
    'name'    => 'SNS_SCENARIO_NOEUD_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_NOEUD_SEUIL',
    'columns' => [
        'SCENARIO_NOEUD_ID',
    ],
];

//@formatter:on
