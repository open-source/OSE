<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_REFERENTIEL_REF_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_REFERENTIEL',
    'columns' => [
        'FONCTION_REFERENTIEL_ID',
    ],
];

//@formatter:on
