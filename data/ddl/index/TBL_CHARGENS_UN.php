<?php

//@formatter:off

return [
    'name'    => 'TBL_CHARGENS_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'ANNEE_ID',
        'NOEUD_ID',
        'SCENARIO_ID',
        'TYPE_HEURES_ID',
        'TYPE_INTERVENTION_ID',
        'ELEMENT_PEDAGOGIQUE_ID',
        'ETAPE_ID',
        'ETAPE_ENS_ID',
        'STRUCTURE_ID',
        'GROUPE_TYPE_FORMATION_ID',
    ],
];

//@formatter:on
