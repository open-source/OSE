<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_INTERVENTION',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
