<?php

//@formatter:off

return [
    'name'    => 'TBL_AGR_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_AGREMENT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
