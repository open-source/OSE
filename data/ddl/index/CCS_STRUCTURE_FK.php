<?php

//@formatter:off

return [
    'name'    => 'CCS_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_STRUCTURE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
