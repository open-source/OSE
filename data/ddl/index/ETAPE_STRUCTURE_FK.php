<?php

//@formatter:off

return [
    'name'    => 'ETAPE_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'ETAPE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
