<?php

//@formatter:off

return [
    'name'    => 'TBL_CLOTURE_REALISE_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CLOTURE_REALISE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
