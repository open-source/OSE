<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_EP_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_EP',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
