<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_EP_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_EP',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
