<?php

//@formatter:off

return [
    'name'    => 'SNE_TYPE_HEURES_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_NOEUD_EFFECTIF',
    'columns' => [
        'TYPE_HEURES_ID',
    ],
];

//@formatter:on
