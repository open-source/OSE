<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_SIGNATUREFLOW',
    'unique'  => TRUE,
    'table'   => 'UNICAEN_SIGNATURE_SIGNATUREFLOW',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
