<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_PRIME_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_MISSION_PRIME',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
