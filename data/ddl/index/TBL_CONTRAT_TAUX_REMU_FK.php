<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_TAUX_REMU_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'TAUX_REMU_ID',
    ],
];

//@formatter:on
