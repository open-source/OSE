<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_TAUX_REMU_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'TAUX_REMU_ID',
    ],
];

//@formatter:on
