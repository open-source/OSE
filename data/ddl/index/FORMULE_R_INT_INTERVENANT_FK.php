<?php

//@formatter:off

return [
    'name'    => 'FORMULE_R_INT_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_INTERVENANT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
