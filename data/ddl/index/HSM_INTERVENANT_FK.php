<?php

//@formatter:off

return [
    'name'    => 'HSM_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'HISTO_INTERVENANT_SERVICE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
