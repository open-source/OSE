<?php

//@formatter:off

return [
    'name'    => 'WE_PREC_WE_FK',
    'unique'  => FALSE,
    'table'   => 'WF_ETAPE_DEP',
    'columns' => [
        'ETAPE_PREC_ID',
    ],
];

//@formatter:on
