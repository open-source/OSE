<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_CONTRAT_PARENT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'CONTRAT_PARENT_ID',
    ],
];

//@formatter:on
