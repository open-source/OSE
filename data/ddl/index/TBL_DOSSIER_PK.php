<?php

//@formatter:off

return [
    'name'    => 'TBL_DOSSIER_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_DOSSIER',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
