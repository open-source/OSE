<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STATUT_UN',
    'unique'  => TRUE,
    'table'   => 'PLAFOND_STATUT',
    'columns' => [
        'STATUT_ID',
        'PLAFOND_ID',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
