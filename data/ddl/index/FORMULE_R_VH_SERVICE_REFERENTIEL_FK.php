<?php

//@formatter:off

return [
    'name'    => 'FORMULE_R_VH_SERVICE_REFERENTIEL_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_VOLUME_HORAIRE',
    'columns' => [
        'SERVICE_REFERENTIEL_ID',
    ],
];

//@formatter:on
