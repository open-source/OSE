<?php

//@formatter:off

return [
    'name'    => 'TYPE_NOTE_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_NOTE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
