<?php

//@formatter:off

return [
    'name'    => 'TPJS_TYPE_PIECE_JOINTE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_PIECE_JOINTE_STATUT',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
    ],
];

//@formatter:on
