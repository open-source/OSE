<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_ADR_NUM_COMPL_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'ADRESSE_NUMERO_COMPL_ID',
    ],
];

//@formatter:on
