<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_RECIPIENT',
    'unique'  => TRUE,
    'table'   => 'UNICAEN_SIGNATURE_RECIPIENT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
