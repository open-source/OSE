<?php

//@formatter:off

return [
    'name'    => 'PERIMETRE_PK',
    'unique'  => TRUE,
    'table'   => 'PERIMETRE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
