<?php

//@formatter:off

return [
    'name'    => 'WE_SUIV_WE_FK',
    'unique'  => FALSE,
    'table'   => 'WF_ETAPE_DEP',
    'columns' => [
        'ETAPE_SUIV_ID',
    ],
];

//@formatter:on
