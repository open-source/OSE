<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_SIGNATUREFLOWSTEP_FK',
    'unique'  => FALSE,
    'table'   => 'UNICAEN_SIGNATURE_SIGNATUREFLOWSTEP',
    'columns' => [
        'SIGNATUREFLOW_ID',
    ],
];

//@formatter:on
