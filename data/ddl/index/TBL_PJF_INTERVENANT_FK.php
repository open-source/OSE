<?php

//@formatter:off

return [
    'name'    => 'TBL_PJF_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE_FOURNIE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
