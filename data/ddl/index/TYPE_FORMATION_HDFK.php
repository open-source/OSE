<?php

//@formatter:off

return [
    'name'    => 'TYPE_FORMATION_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_FORMATION',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
