<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_STRUCTURE_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_INTERVENTION_STRUCTURE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
