<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_TVH',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_CODE',
    ],
];

//@formatter:on
