<?php

//@formatter:off

return [
    'name'    => 'TYPE_AGREMENT_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_AGREMENT',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
