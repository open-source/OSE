<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_REF_PLAFOND_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_REFERENTIEL',
    'columns' => [
        'PLAFOND_ID',
    ],
];

//@formatter:on
