<?php

//@formatter:off

return [
    'name'    => 'INDIC_DIFF_DOSSIER_INT_FK',
    'unique'  => FALSE,
    'table'   => 'INDIC_MODIF_DOSSIER',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
