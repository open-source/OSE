<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'SOURCE_CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
        'STATUT_ID',
    ],
];

//@formatter:on
