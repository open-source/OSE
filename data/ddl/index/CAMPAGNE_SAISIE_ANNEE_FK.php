<?php

//@formatter:off

return [
    'name'    => 'CAMPAGNE_SAISIE_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'CAMPAGNE_SAISIE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
