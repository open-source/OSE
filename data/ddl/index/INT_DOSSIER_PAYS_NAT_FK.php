<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_PAYS_NAT_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'PAYS_NATIONALITE_ID',
    ],
];

//@formatter:on
