<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_TYPE_CONTRAT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'TYPE_CONTRAT_ID',
    ],
];

//@formatter:on
