<?php

//@formatter:off

return [
    'name'    => 'VHR_SERVICE_REFERENTIEL_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_REF',
    'columns' => [
        'SERVICE_REFERENTIEL_ID',
    ],
];

//@formatter:on
