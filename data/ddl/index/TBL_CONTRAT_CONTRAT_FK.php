<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_CONTRAT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'CONTRAT_ID',
    ],
];

//@formatter:on
