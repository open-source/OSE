<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
