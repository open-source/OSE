<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_NOEUD__UN',
    'unique'  => TRUE,
    'table'   => 'SCENARIO_NOEUD',
    'columns' => [
        'SCENARIO_ID',
        'NOEUD_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
