<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_TYPE_INT',
    'unique'  => FALSE,
    'table'   => 'TBL_WORKFLOW',
    'columns' => [
        'TYPE_INTERVENANT_CODE',
    ],
];

//@formatter:on
