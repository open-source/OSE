<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
