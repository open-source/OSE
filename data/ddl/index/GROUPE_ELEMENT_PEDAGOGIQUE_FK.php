<?php

//@formatter:off

return [
    'name'    => 'GROUPE_ELEMENT_PEDAGOGIQUE_FK',
    'unique'  => FALSE,
    'table'   => 'GROUPE',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
