<?php

//@formatter:off

return [
    'name'    => 'OFFRE_EMPLOI_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'OFFRE_EMPLOI',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
