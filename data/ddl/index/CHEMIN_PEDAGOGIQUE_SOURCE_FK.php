<?php

//@formatter:off

return [
    'name'    => 'CHEMIN_PEDAGOGIQUE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'CHEMIN_PEDAGOGIQUE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
