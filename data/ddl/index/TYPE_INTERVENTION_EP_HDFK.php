<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_EP_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_EP',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
