<?php

//@formatter:off

return [
    'name'    => 'FORMULE_RESULTAT_INTERVENANT_PK',
    'unique'  => TRUE,
    'table'   => 'FORMULE_RESULTAT_INTERVENANT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
