<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_DEROGATION_PLAFOND_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_DEROGATION',
    'columns' => [
        'PLAFOND_ID',
    ],
];

//@formatter:on
