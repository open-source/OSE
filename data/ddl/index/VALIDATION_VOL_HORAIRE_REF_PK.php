<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_VOL_HORAIRE_REF_PK',
    'unique'  => TRUE,
    'table'   => 'VALIDATION_VOL_HORAIRE_REF',
    'columns' => [
        'VALIDATION_ID',
        'VOLUME_HORAIRE_REF_ID',
    ],
];

//@formatter:on
