<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_MISS_ETAT_REALISE_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_MISSION',
    'columns' => [
        'PLAFOND_ETAT_REALISE_ID',
    ],
];

//@formatter:on
