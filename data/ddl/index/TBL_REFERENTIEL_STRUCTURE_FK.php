<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_REFERENTIEL',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
