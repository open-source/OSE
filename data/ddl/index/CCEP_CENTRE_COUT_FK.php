<?php

//@formatter:off

return [
    'name'    => 'CCEP_CENTRE_COUT_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_EP',
    'columns' => [
        'CENTRE_COUT_ID',
    ],
];

//@formatter:on
