<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'MISSION_ID',
    ],
];

//@formatter:on
