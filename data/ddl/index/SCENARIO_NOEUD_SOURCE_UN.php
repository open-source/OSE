<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_NOEUD_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'SCENARIO_NOEUD',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
