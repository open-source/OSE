<?php

//@formatter:off

return [
    'name'    => 'TYPE_AGREMENT__UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_AGREMENT',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
