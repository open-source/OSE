<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_REF_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_REFERENTIEL',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
