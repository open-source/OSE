<?php

//@formatter:off

return [
    'name'    => 'TYPE_CONTRAT_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_CONTRAT',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
