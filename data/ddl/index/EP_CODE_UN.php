<?php

//@formatter:off

return [
    'name'    => 'EP_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
        'ANNEE_ID',
    ],
];

//@formatter:on
