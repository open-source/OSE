<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_SIGNATURE',
    'unique'  => TRUE,
    'table'   => 'UNICAEN_SIGNATURE_SIGNATURE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
