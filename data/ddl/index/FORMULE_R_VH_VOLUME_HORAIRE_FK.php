<?php

//@formatter:off

return [
    'name'    => 'FORMULE_R_VH_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_VOLUME_HORAIRE',
    'columns' => [
        'VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
