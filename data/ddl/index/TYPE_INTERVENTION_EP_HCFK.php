<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_EP_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_EP',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
