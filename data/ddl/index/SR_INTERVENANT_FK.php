<?php

//@formatter:off

return [
    'name'    => 'SR_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
