<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_OBSERVER_FK',
    'unique'  => FALSE,
    'table'   => 'UNICAEN_SIGNATURE_OBSERVER',
    'columns' => [
        'SIGNATURE_ID',
    ],
];

//@formatter:on
