<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_VOLUME_HORAIRE_REF_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'VOLUME_HORAIRE_REF_ID',
    ],
];

//@formatter:on
