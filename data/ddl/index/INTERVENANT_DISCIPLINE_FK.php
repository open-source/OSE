<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_DISCIPLINE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'DISCIPLINE_ID',
    ],
];

//@formatter:on
