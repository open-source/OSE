<?php

//@formatter:off

return [
    'name'    => 'AGREMENT_TYPE_AGREMENT_FK',
    'unique'  => FALSE,
    'table'   => 'AGREMENT',
    'columns' => [
        'TYPE_AGREMENT_ID',
    ],
];

//@formatter:on
