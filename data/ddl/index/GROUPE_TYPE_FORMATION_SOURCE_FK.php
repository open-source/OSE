<?php

//@formatter:off

return [
    'name'    => 'GROUPE_TYPE_FORMATION_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'GROUPE_TYPE_FORMATION',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
