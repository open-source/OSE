<?php

//@formatter:off

return [
    'name'    => 'DOTATION_HMFK',
    'unique'  => FALSE,
    'table'   => 'DOTATION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
