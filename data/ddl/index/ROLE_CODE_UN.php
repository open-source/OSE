<?php

//@formatter:off

return [
    'name'    => 'ROLE_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'ROLE',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
