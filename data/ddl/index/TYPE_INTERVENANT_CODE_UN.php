<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENANT_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_INTERVENANT',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
