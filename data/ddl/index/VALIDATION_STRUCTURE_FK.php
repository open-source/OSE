<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
