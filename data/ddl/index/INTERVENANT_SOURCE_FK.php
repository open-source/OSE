<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
