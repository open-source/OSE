<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_PERIODE_ENS_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'PERIODE_ENS_ID',
    ],
];

//@formatter:on
