<?php

//@formatter:off

return [
    'name'    => 'FORMULE_R_INT_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_INTERVENANT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
