<?php

//@formatter:off

return [
    'name'    => 'CCEP_ELEMENT_PEDAGOGIQUE_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_EP',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
