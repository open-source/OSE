<?php

//@formatter:off

return [
    'name'    => 'TMS_TYPE_MODUL_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'columns' => [
        'TYPE_MODULATEUR_ID',
    ],
];

//@formatter:on
