<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_PRIME_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_MISSION_PRIME',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
