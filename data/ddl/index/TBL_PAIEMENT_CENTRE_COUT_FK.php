<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_CENTRE_COUT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'CENTRE_COUT_ID',
    ],
];

//@formatter:on
