<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_VOLUME_HORAIRE_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'VOLUME_HORAIRE_MISSION_ID',
    ],
];

//@formatter:on
