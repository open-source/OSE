<?php

//@formatter:off

return [
    'name'    => 'TBCH_ETAPE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'ETAPE_ID',
    ],
];

//@formatter:on
