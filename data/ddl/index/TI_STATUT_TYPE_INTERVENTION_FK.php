<?php

//@formatter:off

return [
    'name'    => 'TI_STATUT_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STATUT',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
