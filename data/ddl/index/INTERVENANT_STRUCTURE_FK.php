<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
