<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_DEROGATION_HDFK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_DEROGATION',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
