<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_PEDAGOGIQUE_PERIODE_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'PERIODE_ID',
    ],
];

//@formatter:on
