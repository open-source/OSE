<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_MISSION_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_PLAFOND_MISSION',
    'columns' => [
        'TYPE_MISSION_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'INTERVENANT_ID',
        'ANNEE_ID',
        'PLAFOND_ID',
    ],
];

//@formatter:on
