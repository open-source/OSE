<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_PK',
    'unique'  => TRUE,
    'table'   => 'EMPLOYEUR',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
