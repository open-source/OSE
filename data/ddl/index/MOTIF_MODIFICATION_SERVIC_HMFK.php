<?php

//@formatter:off

return [
    'name'    => 'MOTIF_MODIFICATION_SERVIC_HMFK',
    'unique'  => FALSE,
    'table'   => 'MOTIF_MODIFICATION_SERVICE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
