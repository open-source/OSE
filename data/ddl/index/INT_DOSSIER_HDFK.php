<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_HDFK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
