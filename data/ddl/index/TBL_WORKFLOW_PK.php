<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_WORKFLOW',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
