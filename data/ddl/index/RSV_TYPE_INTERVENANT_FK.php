<?php

//@formatter:off

return [
    'name'    => 'RSV_TYPE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'REGLE_STRUCTURE_VALIDATION',
    'columns' => [
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
