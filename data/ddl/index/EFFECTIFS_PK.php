<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_PK',
    'unique'  => TRUE,
    'table'   => 'EFFECTIFS',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
