<?php

//@formatter:off

return [
    'name'    => 'TYPE_MISSION_TAUX_REMU_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MISSION',
    'columns' => [
        'TAUX_REMU_ID',
    ],
];

//@formatter:on
