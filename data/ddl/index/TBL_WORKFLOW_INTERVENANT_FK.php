<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_WORKFLOW',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
