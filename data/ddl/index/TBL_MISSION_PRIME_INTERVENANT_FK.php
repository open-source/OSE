<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_PRIME_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_MISSION_PRIME',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
