<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_STATUT_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'STATUT_ID',
    ],
];

//@formatter:on
