<?php

//@formatter:off

return [
    'name'    => 'IMPORT_TABLES_PK',
    'unique'  => TRUE,
    'table'   => 'IMPORT_TABLES',
    'columns' => [
        'TABLE_NAME',
    ],
];

//@formatter:on
