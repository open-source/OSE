<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_INTERVENTION',
    'columns' => [
        'HISTO_DESTRUCTION',
        'SOURCE_CODE',
    ],
];

//@formatter:on
