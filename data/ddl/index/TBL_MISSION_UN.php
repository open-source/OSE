<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_MISSION',
    'columns' => [
        'INTERVENANT_ID',
        'MISSION_ID',
    ],
];

//@formatter:on
