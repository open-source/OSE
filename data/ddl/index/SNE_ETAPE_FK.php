<?php

//@formatter:off

return [
    'name'    => 'SNE_ETAPE_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_NOEUD_EFFECTIF',
    'columns' => [
        'ETAPE_ID',
    ],
];

//@formatter:on
