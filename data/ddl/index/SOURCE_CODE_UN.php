<?php

//@formatter:off

return [
    'name'    => 'SOURCE_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'SOURCE',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
