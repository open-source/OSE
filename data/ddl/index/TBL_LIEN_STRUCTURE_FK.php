<?php

//@formatter:off

return [
    'name'    => 'TBL_LIEN_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_LIEN',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
