<?php

//@formatter:off

return [
    'name'    => 'MOTIF_NON_PAIEMENT_HMFK',
    'unique'  => FALSE,
    'table'   => 'MOTIF_NON_PAIEMENT',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
