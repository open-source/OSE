<?php

//@formatter:off

return [
    'name'    => 'TBL_DMEP_LIQUIDATION_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_DMEP_LIQUIDATION',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
