<?php

//@formatter:off

return [
    'name'    => 'TBL_VALIDATION_REFERENTIEL_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_VALIDATION_REFERENTIEL',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
