<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
