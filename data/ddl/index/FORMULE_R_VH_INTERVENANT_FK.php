<?php

//@formatter:off

return [
    'name'    => 'FORMULE_R_VH_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_VOLUME_HORAIRE',
    'columns' => [
        'FORMULE_RESULTAT_INTERVENANT_ID',
    ],
];

//@formatter:on
