<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_SIGNATUREFLOWSTEP',
    'unique'  => TRUE,
    'table'   => 'UNICAEN_SIGNATURE_SIGNATUREFLOWSTEP',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
