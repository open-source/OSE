<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_DOMAINE_FONCTIONNEL_FK',
    'unique'  => FALSE,
    'table'   => 'STRUCTURE',
    'columns' => [
        'DOMAINE_FONCTIONNEL_ID',
    ],
];

//@formatter:on
