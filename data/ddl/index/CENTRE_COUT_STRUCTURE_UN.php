<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_STRUCTURE_UN',
    'unique'  => TRUE,
    'table'   => 'CENTRE_COUT_STRUCTURE',
    'columns' => [
        'CENTRE_COUT_ID',
        'STRUCTURE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
