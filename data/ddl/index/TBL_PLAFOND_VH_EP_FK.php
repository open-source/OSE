<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_VH_EP_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_VOLUME_HORAIRE',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
