<?php

//@formatter:off

return [
    'name'    => 'LIEN_NOEUD_SUP_FK',
    'unique'  => FALSE,
    'table'   => 'LIEN',
    'columns' => [
        'NOEUD_SUP_ID',
    ],
];

//@formatter:on
