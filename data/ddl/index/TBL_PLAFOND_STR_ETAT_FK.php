<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_STR_ETAT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_STRUCTURE',
    'columns' => [
        'PLAFOND_ETAT_ID',
    ],
];

//@formatter:on
