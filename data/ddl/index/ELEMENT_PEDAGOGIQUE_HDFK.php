<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_PEDAGOGIQUE_HDFK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
