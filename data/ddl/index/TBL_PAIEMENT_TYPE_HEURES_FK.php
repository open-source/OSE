<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_TYPE_HEURES_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'TYPE_HEURES_ID',
    ],
];

//@formatter:on
