<?php

//@formatter:off

return [
    'name'    => 'HSM_UTILISATEUR_FK',
    'unique'  => FALSE,
    'table'   => 'HISTO_INTERVENANT_SERVICE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
