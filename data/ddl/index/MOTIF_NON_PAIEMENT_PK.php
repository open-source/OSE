<?php

//@formatter:off

return [
    'name'    => 'MOTIF_NON_PAIEMENT_PK',
    'unique'  => TRUE,
    'table'   => 'MOTIF_NON_PAIEMENT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
