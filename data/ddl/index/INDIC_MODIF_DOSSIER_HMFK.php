<?php

//@formatter:off

return [
    'name'    => 'INDIC_MODIF_DOSSIER_HMFK',
    'unique'  => FALSE,
    'table'   => 'INDIC_MODIF_DOSSIER',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
