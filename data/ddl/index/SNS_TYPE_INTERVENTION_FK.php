<?php

//@formatter:off

return [
    'name'    => 'SNS_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_NOEUD_SEUIL',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
