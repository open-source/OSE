<?php

//@formatter:off

return [
    'name'    => 'CAMPAGNE_SAISIE_UN',
    'unique'  => TRUE,
    'table'   => 'CAMPAGNE_SAISIE',
    'columns' => [
        'ANNEE_ID',
        'TYPE_INTERVENANT_ID',
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
