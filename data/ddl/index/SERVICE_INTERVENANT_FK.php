<?php

//@formatter:off

return [
    'name'    => 'SERVICE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'SERVICE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
