<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_MISSION_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_MISSION',
    'columns' => [
        'TYPE_MISSION_ID',
    ],
];

//@formatter:on
