<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_ELEMENT_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_ELEMENT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
