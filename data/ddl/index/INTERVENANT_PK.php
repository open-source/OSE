<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_PK',
    'unique'  => TRUE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
