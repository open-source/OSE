<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_REF_ETAT_PREVU_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_REFERENTIEL',
    'columns' => [
        'PLAFOND_ETAT_PREVU_ID',
    ],
];

//@formatter:on
