<?php

//@formatter:off

return [
    'name'    => 'TBL_PIECE_JOINTE_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
