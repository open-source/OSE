<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_ETAPE',
    'unique'  => FALSE,
    'table'   => 'TBL_WORKFLOW',
    'columns' => [
        'ETAPE_CODE',
    ],
];

//@formatter:on
