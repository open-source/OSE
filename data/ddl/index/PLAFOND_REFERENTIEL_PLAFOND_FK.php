<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_REFERENTIEL_PLAFOND_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_REFERENTIEL',
    'columns' => [
        'PLAFOND_ID',
    ],
];

//@formatter:on
