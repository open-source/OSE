<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_VH_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_PLAFOND_VOLUME_HORAIRE',
    'columns' => [
        'ANNEE_ID',
        'ELEMENT_PEDAGOGIQUE_ID',
        'TYPE_INTERVENTION_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'INTERVENANT_ID',
        'PLAFOND_ID',
    ],
];

//@formatter:on
