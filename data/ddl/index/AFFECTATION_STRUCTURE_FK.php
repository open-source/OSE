<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
