<?php

//@formatter:off

return [
    'name'    => 'MISE_EN_PAIEMENT_SERVICE_REFERENTIEL_FK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'SERVICE_REFERENTIEL_ID',
    ],
];

//@formatter:on
