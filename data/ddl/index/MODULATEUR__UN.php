<?php

//@formatter:off

return [
    'name'    => 'MODULATEUR__UN',
    'unique'  => TRUE,
    'table'   => 'MODULATEUR',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
