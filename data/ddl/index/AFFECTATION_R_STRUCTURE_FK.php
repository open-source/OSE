<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_R_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION_RECHERCHE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
