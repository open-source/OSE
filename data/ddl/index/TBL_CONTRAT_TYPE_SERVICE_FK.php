<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_TYPE_SERVICE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'TYPE_SERVICE_ID',
    ],
];

//@formatter:on
