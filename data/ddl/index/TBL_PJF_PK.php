<?php

//@formatter:off

return [
    'name'    => 'TBL_PJF_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_PIECE_JOINTE_FOURNIE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
