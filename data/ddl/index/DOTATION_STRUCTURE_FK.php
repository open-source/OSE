<?php

//@formatter:off

return [
    'name'    => 'DOTATION_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'DOTATION',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
