<?php

//@formatter:off

return [
    'name'    => 'TBCH_GROUPE_TYPE_FORMATION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'GROUPE_TYPE_FORMATION_ID',
    ],
];

//@formatter:on
