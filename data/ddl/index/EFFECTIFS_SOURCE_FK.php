<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'EFFECTIFS',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
