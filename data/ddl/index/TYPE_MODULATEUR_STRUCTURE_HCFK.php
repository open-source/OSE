<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_STRUCTURE_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
