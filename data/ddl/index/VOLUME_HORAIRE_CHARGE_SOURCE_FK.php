<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_CHARGE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_CHARGE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
