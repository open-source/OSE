<?php

//@formatter:off

return [
    'name'    => 'TBCH_NOEUD_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'NOEUD_ID',
    ],
];

//@formatter:on
