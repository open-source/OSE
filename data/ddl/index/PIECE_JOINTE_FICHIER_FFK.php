<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE_FICHIER_FFK',
    'unique'  => FALSE,
    'table'   => 'PIECE_JOINTE_FICHIER',
    'columns' => [
        'FICHIER_ID',
    ],
];

//@formatter:on
