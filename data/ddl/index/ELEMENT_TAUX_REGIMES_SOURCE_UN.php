<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_TAUX_REGIMES_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
