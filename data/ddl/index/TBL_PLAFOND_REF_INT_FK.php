<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_REF_INT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_REFERENTIEL',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
