<?php

//@formatter:off

return [
    'name'    => 'MODULATEUR_TYPE_MODULATEUR_FK',
    'unique'  => FALSE,
    'table'   => 'MODULATEUR',
    'columns' => [
        'TYPE_MODULATEUR_ID',
    ],
];

//@formatter:on
