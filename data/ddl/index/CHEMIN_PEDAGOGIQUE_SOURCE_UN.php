<?php

//@formatter:off

return [
    'name'    => 'CHEMIN_PEDAGOGIQUE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'CHEMIN_PEDAGOGIQUE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
