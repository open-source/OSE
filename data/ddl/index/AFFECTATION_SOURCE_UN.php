<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'AFFECTATION',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
