<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_SIGNATURE_OBSERVER',
    'unique'  => TRUE,
    'table'   => 'UNICAEN_SIGNATURE_OBSERVER',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
