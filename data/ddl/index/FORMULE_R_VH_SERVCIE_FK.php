<?php

//@formatter:off

return [
    'name'    => 'FORMULE_R_VH_SERVCIE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_VOLUME_HORAIRE',
    'columns' => [
        'SERVICE_ID',
    ],
];

//@formatter:on
