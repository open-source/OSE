<?php

//@formatter:off

return [
    'name'    => 'SERVICE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'SERVICE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
