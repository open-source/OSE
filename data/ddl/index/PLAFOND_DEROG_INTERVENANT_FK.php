<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_DEROG_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_DEROGATION',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
