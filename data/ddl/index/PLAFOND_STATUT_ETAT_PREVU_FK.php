<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STATUT_ETAT_PREVU_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STATUT',
    'columns' => [
        'PLAFOND_ETAT_PREVU_ID',
    ],
];

//@formatter:on
