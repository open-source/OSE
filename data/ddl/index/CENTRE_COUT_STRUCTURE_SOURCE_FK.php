<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_STRUCTURE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_STRUCTURE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
