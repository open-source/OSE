<?php

//@formatter:off

return [
    'name'    => 'PRIVILEGE_PK',
    'unique'  => TRUE,
    'table'   => 'PRIVILEGE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
