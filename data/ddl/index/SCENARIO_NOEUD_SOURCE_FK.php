<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_NOEUD_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_NOEUD',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
