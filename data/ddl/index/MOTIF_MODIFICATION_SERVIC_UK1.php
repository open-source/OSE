<?php

//@formatter:off

return [
    'name'    => 'MOTIF_MODIFICATION_SERVIC_UK1',
    'unique'  => TRUE,
    'table'   => 'MOTIF_MODIFICATION_SERVICE',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
