<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_CENTRE_COUT_FK',
    'unique'  => FALSE,
    'table'   => 'STRUCTURE',
    'columns' => [
        'CENTRE_COUT_ID',
    ],
];

//@formatter:on
