<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_MISS_ETAT_PREVU_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_MISSION',
    'columns' => [
        'PLAFOND_ETAT_PREVU_ID',
    ],
];

//@formatter:on
