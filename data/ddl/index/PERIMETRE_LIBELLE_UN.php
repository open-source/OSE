<?php

//@formatter:off

return [
    'name'    => 'PERIMETRE_LIBELLE_UN',
    'unique'  => TRUE,
    'table'   => 'PERIMETRE',
    'columns' => [
        'LIBELLE',
    ],
];

//@formatter:on
