<?php

//@formatter:off

return [
    'name'    => 'SRFR_FK',
    'unique'  => FALSE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'FONCTION_ID',
    ],
];

//@formatter:on
