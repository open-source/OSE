<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_STRUCTURE_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
