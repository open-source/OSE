<?php

//@formatter:off

return [
    'name'    => 'MEP_DOMAINE_FONCTIONNEL_FK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'DOMAINE_FONCTIONNEL_ID',
    ],
];

//@formatter:on
