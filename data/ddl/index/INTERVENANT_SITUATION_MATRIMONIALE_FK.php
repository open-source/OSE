<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_SITUATION_MATRIMONIALE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'SITUATION_MATRIMONIALE_ID',
    ],
];

//@formatter:on
