<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_REF_HMFK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_REF',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
