<?php

//@formatter:off

return [
    'name'    => 'TBL_PJD_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE_DEMANDE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
