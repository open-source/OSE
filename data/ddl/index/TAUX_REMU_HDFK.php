<?php

//@formatter:off

return [
    'name'    => 'TAUX_REMU_HDFK',
    'unique'  => FALSE,
    'table'   => 'TAUX_REMU',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
