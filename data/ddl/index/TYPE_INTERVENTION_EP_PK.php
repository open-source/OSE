<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_EP_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_INTERVENTION_EP',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
