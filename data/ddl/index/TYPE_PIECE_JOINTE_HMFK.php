<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_PIECE_JOINTE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
