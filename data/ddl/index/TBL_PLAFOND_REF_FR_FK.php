<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_REF_FR_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_REFERENTIEL',
    'columns' => [
        'FONCTION_REFERENTIEL_ID',
    ],
];

//@formatter:on
