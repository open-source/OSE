<?php

//@formatter:off

return [
    'name'    => 'UTILISATEUR_PK',
    'unique'  => TRUE,
    'table'   => 'UTILISATEUR',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
