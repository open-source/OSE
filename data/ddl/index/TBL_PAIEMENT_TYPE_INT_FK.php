<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_TYPE_INT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
