<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_MISSION_PK',
    'unique'  => TRUE,
    'table'   => 'VALIDATION_MISSION',
    'columns' => [
        'VALIDATION_ID',
        'MISSION_ID',
    ],
];

//@formatter:on
