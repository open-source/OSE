<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_CONTRAT_FK',
    'unique'  => FALSE,
    'table'   => 'CONTRAT',
    'columns' => [
        'CONTRAT_ID',
    ],
];

//@formatter:on
