<?php

//@formatter:off

return [
    'name'    => 'TYPE_AGREMENT_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_AGREMENT',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
