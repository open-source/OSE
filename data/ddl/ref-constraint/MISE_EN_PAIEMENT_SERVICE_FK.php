<?php

//@formatter:off

return [
    'name'        => 'MISE_EN_PAIEMENT_SERVICE_FK',
    'table'       => 'MISE_EN_PAIEMENT',
    'rtable'      => 'SERVICE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SERVICE_ID' => 'ID',
    ],
];

//@formatter:on
