<?php

//@formatter:off

return [
    'name'        => 'TIS_ANNEE_DEBUT_FK',
    'table'       => 'TYPE_INTERVENTION_STRUCTURE',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_DEBUT_ID' => 'ID',
    ],
];

//@formatter:on
