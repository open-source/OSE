<?php

//@formatter:off

return [
    'name'        => 'ROLE_HCFK',
    'table'       => 'ROLE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
