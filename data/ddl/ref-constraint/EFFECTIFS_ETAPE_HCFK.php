<?php

//@formatter:off

return [
    'name'        => 'EFFECTIFS_ETAPE_HCFK',
    'table'       => 'EFFECTIFS_ETAPE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
