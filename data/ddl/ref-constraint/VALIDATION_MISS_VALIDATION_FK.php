<?php

//@formatter:off

return [
    'name'        => 'VALIDATION_MISS_VALIDATION_FK',
    'table'       => 'VALIDATION_MISSION',
    'rtable'      => 'VALIDATION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
