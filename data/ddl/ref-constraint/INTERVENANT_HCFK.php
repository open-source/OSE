<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_HCFK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
