<?php

//@formatter:off

return [
    'name'        => 'TBL_PJ_PIECE_JOINTE_FK',
    'table'       => 'TBL_PIECE_JOINTE',
    'rtable'      => 'PIECE_JOINTE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'PIECE_JOINTE_ID' => 'ID',
    ],
];

//@formatter:on
