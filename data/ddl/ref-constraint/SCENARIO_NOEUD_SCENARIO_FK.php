<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_NOEUD_SCENARIO_FK',
    'table'       => 'SCENARIO_NOEUD',
    'rtable'      => 'SCENARIO',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'SCENARIO_ID' => 'ID',
    ],
];

//@formatter:on
