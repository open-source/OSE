<?php

//@formatter:off

return [
    'name'        => 'CORPS_HDFK',
    'table'       => 'CORPS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
