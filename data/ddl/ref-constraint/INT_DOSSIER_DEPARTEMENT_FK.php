<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_DEPARTEMENT_FK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'DEPARTEMENT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'DEPARTEMENT_NAISSANCE_ID' => 'ID',
    ],
];

//@formatter:on
