<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_HMFK',
    'table'       => 'SCENARIO',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
