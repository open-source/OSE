<?php

//@formatter:off

return [
    'name'        => 'CANDIDATURE_HMFK',
    'table'       => 'CANDIDATURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
