<?php

//@formatter:off

return [
    'name'        => 'SEUIL_CH_SCENARIO_FK',
    'table'       => 'SEUIL_CHARGE',
    'rtable'      => 'SCENARIO',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SCENARIO_ID' => 'ID',
    ],
];

//@formatter:on
