<?php

//@formatter:off

return [
    'name'        => 'TYPE_PIECE_JOINTE_HCFK',
    'table'       => 'TYPE_PIECE_JOINTE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
