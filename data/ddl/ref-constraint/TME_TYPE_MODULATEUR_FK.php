<?php

//@formatter:off

return [
    'name'        => 'TME_TYPE_MODULATEUR_FK',
    'table'       => 'TYPE_MODULATEUR_EP',
    'rtable'      => 'TYPE_MODULATEUR',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_MODULATEUR_ID' => 'ID',
    ],
];

//@formatter:on
