<?php

//@formatter:off

return [
    'name'        => 'TBL_AGR_STRUCTURE_FK',
    'table'       => 'TBL_AGREMENT',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
