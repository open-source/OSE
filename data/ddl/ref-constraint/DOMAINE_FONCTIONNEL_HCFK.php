<?php

//@formatter:off

return [
    'name'        => 'DOMAINE_FONCTIONNEL_HCFK',
    'table'       => 'DOMAINE_FONCTIONNEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
