<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_REFERENTIEL_HCFK',
    'table'       => 'PLAFOND_REFERENTIEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
