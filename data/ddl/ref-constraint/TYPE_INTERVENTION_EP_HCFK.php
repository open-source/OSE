<?php

//@formatter:off

return [
    'name'        => 'TYPE_INTERVENTION_EP_HCFK',
    'table'       => 'TYPE_INTERVENTION_EP',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
