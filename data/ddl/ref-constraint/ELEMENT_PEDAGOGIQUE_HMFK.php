<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_PEDAGOGIQUE_HMFK',
    'table'       => 'ELEMENT_PEDAGOGIQUE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
