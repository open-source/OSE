<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_LIEN_SOURCE_FK',
    'table'       => 'SCENARIO_LIEN',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
