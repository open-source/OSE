<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_PEDAGOGIQUE_HDFK',
    'table'       => 'ELEMENT_PEDAGOGIQUE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
