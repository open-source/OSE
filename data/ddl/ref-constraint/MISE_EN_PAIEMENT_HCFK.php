<?php

//@formatter:off

return [
    'name'        => 'MISE_EN_PAIEMENT_HCFK',
    'table'       => 'MISE_EN_PAIEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
