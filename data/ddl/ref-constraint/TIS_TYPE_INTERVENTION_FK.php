<?php

//@formatter:off

return [
    'name'        => 'TIS_TYPE_INTERVENTION_FK',
    'table'       => 'TYPE_INTERVENTION_STRUCTURE',
    'rtable'      => 'TYPE_INTERVENTION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENTION_ID' => 'ID',
    ],
];

//@formatter:on
