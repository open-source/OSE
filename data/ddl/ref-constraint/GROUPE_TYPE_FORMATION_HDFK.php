<?php

//@formatter:off

return [
    'name'        => 'GROUPE_TYPE_FORMATION_HDFK',
    'table'       => 'GROUPE_TYPE_FORMATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
