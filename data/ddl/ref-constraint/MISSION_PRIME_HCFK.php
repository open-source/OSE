<?php

//@formatter:off

return [
    'name'        => 'MISSION_PRIME_HCFK',
    'table'       => 'MISSION_PRIME',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
