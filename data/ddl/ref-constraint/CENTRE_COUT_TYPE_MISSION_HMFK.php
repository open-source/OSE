<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_TYPE_MISSION_HMFK',
    'table'       => 'CENTRE_COUT_TYPE_MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
