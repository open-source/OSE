<?php

//@formatter:off

return [
    'name'        => 'VOLUME_HORAIRE_HCFK',
    'table'       => 'VOLUME_HORAIRE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
