<?php

//@formatter:off

return [
    'name'        => 'NOTE_TYPE_NOTE_FK',
    'table'       => 'NOTE',
    'rtable'      => 'TYPE_NOTE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_NOTE_ID' => 'ID',
    ],
];

//@formatter:on
