<?php

//@formatter:off

return [
    'name'        => 'VOLUME_HORAIRE_MISSION_SOURCE_FK',
    'table'       => 'VOLUME_HORAIRE_MISSION',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
