<?php

//@formatter:off

return [
    'name'        => 'VH_CHARGE_HMFK',
    'table'       => 'VOLUME_HORAIRE_CHARGE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
