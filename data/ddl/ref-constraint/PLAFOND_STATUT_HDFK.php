<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STATUT_HDFK',
    'table'       => 'PLAFOND_STATUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
