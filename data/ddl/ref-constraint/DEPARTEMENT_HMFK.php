<?php

//@formatter:off

return [
    'name'        => 'DEPARTEMENT_HMFK',
    'table'       => 'DEPARTEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
