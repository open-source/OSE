<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_RECHERCHE_SOURCE_FK',
    'table'       => 'AFFECTATION_RECHERCHE',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
