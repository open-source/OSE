<?php

//@formatter:off

return [
    'name'        => 'TBL_AGR_AGREMENT_FK',
    'table'       => 'TBL_AGREMENT',
    'rtable'      => 'AGREMENT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'AGREMENT_ID' => 'ID',
    ],
];

//@formatter:on
