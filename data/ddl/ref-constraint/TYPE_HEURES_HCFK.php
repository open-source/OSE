<?php

//@formatter:off

return [
    'name'        => 'TYPE_HEURES_HCFK',
    'table'       => 'TYPE_HEURES',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
