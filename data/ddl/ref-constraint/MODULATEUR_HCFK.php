<?php

//@formatter:off

return [
    'name'        => 'MODULATEUR_HCFK',
    'table'       => 'MODULATEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
