<?php

//@formatter:off

return [
    'name'        => 'TYPE_MODULATEUR_STRUCTURE_HMFK',
    'table'       => 'TYPE_MODULATEUR_STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
