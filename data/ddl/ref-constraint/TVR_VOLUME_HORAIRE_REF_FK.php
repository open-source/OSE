<?php

//@formatter:off

return [
    'name'        => 'TVR_VOLUME_HORAIRE_REF_FK',
    'table'       => 'TBL_VALIDATION_REFERENTIEL',
    'rtable'      => 'VOLUME_HORAIRE_REF',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'VOLUME_HORAIRE_REF_ID' => 'ID',
    ],
];

//@formatter:on
