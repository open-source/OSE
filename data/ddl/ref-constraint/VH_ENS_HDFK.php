<?php

//@formatter:off

return [
    'name'        => 'VH_ENS_HDFK',
    'table'       => 'VOLUME_HORAIRE_ENS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
