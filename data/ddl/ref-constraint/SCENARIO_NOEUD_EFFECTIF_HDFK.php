<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_NOEUD_EFFECTIF_HDFK',
    'table'       => 'SCENARIO_NOEUD_EFFECTIF',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
