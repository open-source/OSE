<?php

//@formatter:off

return [
    'name'        => 'MISSION_PRIME_INTERVENANT_FK',
    'table'       => 'MISSION_PRIME',
    'rtable'      => 'INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
