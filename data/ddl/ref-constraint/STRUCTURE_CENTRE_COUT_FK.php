<?php

//@formatter:off

return [
    'name'        => 'STRUCTURE_CENTRE_COUT_FK',
    'table'       => 'STRUCTURE',
    'rtable'      => 'CENTRE_COUT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'CENTRE_COUT_ID' => 'ID',
    ],
];

//@formatter:on
