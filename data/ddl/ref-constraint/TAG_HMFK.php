<?php

//@formatter:off

return [
    'name'        => 'TAG_HMFK',
    'table'       => 'TAG',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
