<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_HMFK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
