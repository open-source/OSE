<?php

//@formatter:off

return [
    'name'        => 'FONCTION_REFERENTIEL_ANNEE_FK',
    'table'       => 'FONCTION_REFERENTIEL',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
