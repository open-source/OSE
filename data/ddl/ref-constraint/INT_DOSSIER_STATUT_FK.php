<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_STATUT_FK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'STATUT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STATUT_ID' => 'ID',
    ],
];

//@formatter:on
