<?php

//@formatter:off

return [
    'name'        => 'ETABLISSEMENT_HDFK',
    'table'       => 'ETABLISSEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
