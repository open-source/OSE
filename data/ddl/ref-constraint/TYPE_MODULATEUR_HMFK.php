<?php

//@formatter:off

return [
    'name'        => 'TYPE_MODULATEUR_HMFK',
    'table'       => 'TYPE_MODULATEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
