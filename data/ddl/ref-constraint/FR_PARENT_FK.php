<?php

//@formatter:off

return [
    'name'        => 'FR_PARENT_FK',
    'table'       => 'FONCTION_REFERENTIEL',
    'rtable'      => 'FONCTION_REFERENTIEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PARENT_ID' => 'ID',
    ],
];

//@formatter:on
