<?php

//@formatter:off

return [
    'name'        => 'TBL_DMLIQ_ANNEE_FK',
    'table'       => 'TBL_DMEP_LIQUIDATION',
    'rtable'      => 'ANNEE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
