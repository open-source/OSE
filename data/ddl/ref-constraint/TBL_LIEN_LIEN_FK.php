<?php

//@formatter:off

return [
    'name'        => 'TBL_LIEN_LIEN_FK',
    'table'       => 'TBL_LIEN',
    'rtable'      => 'LIEN',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'LIEN_ID' => 'ID',
    ],
];

//@formatter:on
