<?php

//@formatter:off

return [
    'name'        => 'STRUCTURE_DOMAINE_FONCTIONNEL_FK',
    'table'       => 'STRUCTURE',
    'rtable'      => 'DOMAINE_FONCTIONNEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'DOMAINE_FONCTIONNEL_ID' => 'ID',
    ],
];

//@formatter:on
