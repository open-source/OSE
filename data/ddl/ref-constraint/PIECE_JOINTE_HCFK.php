<?php

//@formatter:off

return [
    'name'        => 'PIECE_JOINTE_HCFK',
    'table'       => 'PIECE_JOINTE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
