<?php

//@formatter:off

return [
    'name'        => 'ETAPE_HDFK',
    'table'       => 'ETAPE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
