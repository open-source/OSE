<?php

//@formatter:off

return [
    'name'        => 'TPJS_STATUT_FK',
    'table'       => 'TYPE_PIECE_JOINTE_STATUT',
    'rtable'      => 'STATUT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'STATUT_ID' => 'ID',
    ],
];

//@formatter:on
