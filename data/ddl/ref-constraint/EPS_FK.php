<?php

//@formatter:off

return [
    'name'        => 'EPS_FK',
    'table'       => 'ELEMENT_PEDAGOGIQUE',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
