<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_HCFK',
    'table'       => 'AFFECTATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
