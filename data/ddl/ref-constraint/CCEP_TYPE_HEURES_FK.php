<?php

//@formatter:off

return [
    'name'        => 'CCEP_TYPE_HEURES_FK',
    'table'       => 'CENTRE_COUT_EP',
    'rtable'      => 'TYPE_HEURES',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_HEURES_ID' => 'ID',
    ],
];

//@formatter:on
