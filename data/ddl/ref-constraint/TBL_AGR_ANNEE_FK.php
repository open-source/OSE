<?php

//@formatter:off

return [
    'name'        => 'TBL_AGR_ANNEE_FK',
    'table'       => 'TBL_AGREMENT',
    'rtable'      => 'ANNEE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
