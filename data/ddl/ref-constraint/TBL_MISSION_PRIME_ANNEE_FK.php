<?php

//@formatter:off

return [
    'name'        => 'TBL_MISSION_PRIME_ANNEE_FK',
    'table'       => 'TBL_MISSION_PRIME',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
