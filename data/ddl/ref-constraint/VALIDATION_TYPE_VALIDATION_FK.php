<?php

//@formatter:off

return [
    'name'        => 'VALIDATION_TYPE_VALIDATION_FK',
    'table'       => 'VALIDATION',
    'rtable'      => 'TYPE_VALIDATION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
