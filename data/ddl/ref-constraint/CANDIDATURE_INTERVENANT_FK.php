<?php

//@formatter:off

return [
    'name'        => 'CANDIDATURE_INTERVENANT_FK',
    'table'       => 'CANDIDATURE',
    'rtable'      => 'INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
