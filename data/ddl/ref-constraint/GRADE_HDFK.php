<?php

//@formatter:off

return [
    'name'        => 'GRADE_HDFK',
    'table'       => 'GRADE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
