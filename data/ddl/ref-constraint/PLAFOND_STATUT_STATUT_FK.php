<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STATUT_STATUT_FK',
    'table'       => 'PLAFOND_STATUT',
    'rtable'      => 'STATUT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'STATUT_ID' => 'ID',
    ],
];

//@formatter:on
