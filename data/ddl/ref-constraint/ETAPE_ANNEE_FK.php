<?php

//@formatter:off

return [
    'name'        => 'ETAPE_ANNEE_FK',
    'table'       => 'ETAPE',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
