<?php

//@formatter:off

return [
    'name'        => 'FORMULE_R_INT_INTERVENANT_FK',
    'table'       => 'FORMULE_RESULTAT_INTERVENANT',
    'rtable'      => 'INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
