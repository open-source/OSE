<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_MODULATEUR_HMFK',
    'table'       => 'ELEMENT_MODULATEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
