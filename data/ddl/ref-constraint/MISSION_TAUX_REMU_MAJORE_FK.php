<?php

//@formatter:off

return [
    'name'        => 'MISSION_TAUX_REMU_MAJORE_FK',
    'table'       => 'MISSION',
    'rtable'      => 'TAUX_REMU',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TAUX_REMU_MAJORE_ID' => 'ID',
    ],
];

//@formatter:on
