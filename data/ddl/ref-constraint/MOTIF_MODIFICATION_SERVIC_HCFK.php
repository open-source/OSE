<?php

//@formatter:off

return [
    'name'        => 'MOTIF_MODIFICATION_SERVIC_HCFK',
    'table'       => 'MOTIF_MODIFICATION_SERVICE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
