<?php

//@formatter:off

return [
    'name'        => 'VOLUME_HORAIRE_MISSION_HDFK',
    'table'       => 'VOLUME_HORAIRE_MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
