<?php

//@formatter:off

return [
    'name'        => 'TYPE_MODULATEUR_EP_SOURCE_FK',
    'table'       => 'TYPE_MODULATEUR_EP',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
