<?php

//@formatter:off

return [
    'name'        => 'LIEN_HMFK',
    'table'       => 'LIEN',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
