<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STATUT_HCFK',
    'table'       => 'PLAFOND_STATUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
