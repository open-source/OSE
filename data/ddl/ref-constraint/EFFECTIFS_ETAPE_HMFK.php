<?php

//@formatter:off

return [
    'name'        => 'EFFECTIFS_ETAPE_HMFK',
    'table'       => 'EFFECTIFS_ETAPE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
