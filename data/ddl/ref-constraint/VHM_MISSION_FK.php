<?php

//@formatter:off

return [
    'name'        => 'VHM_MISSION_FK',
    'table'       => 'VOLUME_HORAIRE_MISSION',
    'rtable'      => 'MISSION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'MISSION_ID' => 'ID',
    ],
];

//@formatter:on
