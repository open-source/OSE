<?php

//@formatter:off

return [
    'name'        => 'INDIC_MODIF_DOSSIER_HCFK',
    'table'       => 'INDIC_MODIF_DOSSIER',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
