<?php

//@formatter:off

return [
    'name'        => 'TBL_PAIEMENT_CENTRE_COUT_FK',
    'table'       => 'TBL_PAIEMENT',
    'rtable'      => 'CENTRE_COUT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'CENTRE_COUT_ID' => 'ID',
    ],
];

//@formatter:on
