<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_REFERENTIEL_REF_FK',
    'table'       => 'PLAFOND_REFERENTIEL',
    'rtable'      => 'FONCTION_REFERENTIEL',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'FONCTION_REFERENTIEL_ID' => 'ID',
    ],
];

//@formatter:on
