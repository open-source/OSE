<?php

//@formatter:off

return [
    'name'        => 'TYPE_INTERVENTION_EP_HDFK',
    'table'       => 'TYPE_INTERVENTION_EP',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
