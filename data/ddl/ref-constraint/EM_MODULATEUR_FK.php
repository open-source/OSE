<?php

//@formatter:off

return [
    'name'        => 'EM_MODULATEUR_FK',
    'table'       => 'ELEMENT_MODULATEUR',
    'rtable'      => 'MODULATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'MODULATEUR_ID' => 'ID',
    ],
];

//@formatter:on
