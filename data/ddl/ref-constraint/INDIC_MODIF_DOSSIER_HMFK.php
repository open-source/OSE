<?php

//@formatter:off

return [
    'name'        => 'INDIC_MODIF_DOSSIER_HMFK',
    'table'       => 'INDIC_MODIF_DOSSIER',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
