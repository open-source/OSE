<?php

//@formatter:off

return [
    'name'        => 'SEUIL_CHARGE_ANNEE_FK',
    'table'       => 'SEUIL_CHARGE',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
