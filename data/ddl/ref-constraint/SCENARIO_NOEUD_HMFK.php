<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_NOEUD_HMFK',
    'table'       => 'SCENARIO_NOEUD',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
