<?php

//@formatter:off

return [
    'name'        => 'EFFECTIFS_FK',
    'table'       => 'EFFECTIFS',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
