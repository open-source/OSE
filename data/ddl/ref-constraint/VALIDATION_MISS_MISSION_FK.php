<?php

//@formatter:off

return [
    'name'        => 'VALIDATION_MISS_MISSION_FK',
    'table'       => 'VALIDATION_MISSION',
    'rtable'      => 'MISSION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'MISSION_ID' => 'ID',
    ],
];

//@formatter:on
