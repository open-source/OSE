<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STRUCTURE_HDFK',
    'table'       => 'PLAFOND_STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
