<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_TAUX_REGIMES_SOURCE_FK',
    'table'       => 'ELEMENT_TAUX_REGIMES',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
