<?php

//@formatter:off

return [
    'name'        => 'PJ_TYPE_PIECE_JOINTE_FK',
    'table'       => 'PIECE_JOINTE',
    'rtable'      => 'TYPE_PIECE_JOINTE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_PIECE_JOINTE_ID' => 'ID',
    ],
];

//@formatter:on
