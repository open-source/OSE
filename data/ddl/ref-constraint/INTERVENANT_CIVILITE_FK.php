<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_CIVILITE_FK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'CIVILITE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'CIVILITE_ID' => 'ID',
    ],
];

//@formatter:on
