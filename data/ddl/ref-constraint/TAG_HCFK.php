<?php

//@formatter:off

return [
    'name'        => 'TAG_HCFK',
    'table'       => 'TAG',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
