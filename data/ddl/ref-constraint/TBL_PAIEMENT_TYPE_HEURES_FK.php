<?php

//@formatter:off

return [
    'name'        => 'TBL_PAIEMENT_TYPE_HEURES_FK',
    'table'       => 'TBL_PAIEMENT',
    'rtable'      => 'TYPE_HEURES',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_HEURES_ID' => 'ID',
    ],
];

//@formatter:on
