<?php

//@formatter:off

return [
    'name'        => 'RSV_TYPE_VOLUME_HORAIRE_FK',
    'table'       => 'REGLE_STRUCTURE_VALIDATION',
    'rtable'      => 'TYPE_VOLUME_HORAIRE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_VOLUME_HORAIRE_ID' => 'ID',
    ],
];

//@formatter:on
