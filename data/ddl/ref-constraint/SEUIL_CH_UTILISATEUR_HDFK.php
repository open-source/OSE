<?php

//@formatter:off

return [
    'name'        => 'SEUIL_CH_UTILISATEUR_HDFK',
    'table'       => 'SEUIL_CHARGE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
