<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STATUT_PLAFOND_FK',
    'table'       => 'PLAFOND_STATUT',
    'rtable'      => 'PLAFOND',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ID' => 'ID',
    ],
];

//@formatter:on
