<?php

//@formatter:off

return [
    'name'        => 'GRADE_HCFK',
    'table'       => 'GRADE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
