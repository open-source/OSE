<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_STRUCTURE_FK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
