<?php

//@formatter:off

return [
    'name'        => 'STRUCTURE_ADR_VOIRIE_FK',
    'table'       => 'STRUCTURE',
    'rtable'      => 'VOIRIE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ADRESSE_VOIRIE_ID' => 'ID',
    ],
];

//@formatter:on
