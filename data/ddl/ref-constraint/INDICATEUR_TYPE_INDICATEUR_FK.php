<?php

//@formatter:off

return [
    'name'        => 'INDICATEUR_TYPE_INDICATEUR_FK',
    'table'       => 'INDICATEUR',
    'rtable'      => 'TYPE_INDICATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_INDICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
