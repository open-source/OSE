<?php

//@formatter:off

return [
    'name'        => 'ETAPE_TYPE_FORMATION_FK',
    'table'       => 'ETAPE',
    'rtable'      => 'TYPE_FORMATION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_FORMATION_ID' => 'ID',
    ],
];

//@formatter:on
