<?php

//@formatter:off

return [
    'name'        => 'VH_ENS_HCFK',
    'table'       => 'VOLUME_HORAIRE_ENS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
