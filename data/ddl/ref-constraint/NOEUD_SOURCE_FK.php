<?php

//@formatter:off

return [
    'name'        => 'NOEUD_SOURCE_FK',
    'table'       => 'NOEUD',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
