<?php

//@formatter:off

return [
    'name'        => 'MISSION_PRIME_HMFK',
    'table'       => 'MISSION_PRIME',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
