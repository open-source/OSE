<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_SOURCE_FK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
