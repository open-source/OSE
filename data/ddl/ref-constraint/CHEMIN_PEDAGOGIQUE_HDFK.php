<?php

//@formatter:off

return [
    'name'        => 'CHEMIN_PEDAGOGIQUE_HDFK',
    'table'       => 'CHEMIN_PEDAGOGIQUE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
