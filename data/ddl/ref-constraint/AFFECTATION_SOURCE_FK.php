<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_SOURCE_FK',
    'table'       => 'AFFECTATION',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
