<?php

//@formatter:off

return [
    'name'        => 'EFFECTIFS_HCFK',
    'table'       => 'EFFECTIFS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
