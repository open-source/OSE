<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_NOEUD_EFFECTIF_SOURCE_FK',
    'table'       => 'SCENARIO_NOEUD_EFFECTIF',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
