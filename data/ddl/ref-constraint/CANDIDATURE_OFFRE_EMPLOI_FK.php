<?php

//@formatter:off

return [
    'name'        => 'CANDIDATURE_OFFRE_EMPLOI_FK',
    'table'       => 'CANDIDATURE',
    'rtable'      => 'OFFRE_EMPLOI',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'OFFRE_EMPLOI_ID' => 'ID',
    ],
];

//@formatter:on
