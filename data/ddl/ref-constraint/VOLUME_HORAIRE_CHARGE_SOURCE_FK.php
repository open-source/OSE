<?php

//@formatter:off

return [
    'name'        => 'VOLUME_HORAIRE_CHARGE_SOURCE_FK',
    'table'       => 'VOLUME_HORAIRE_CHARGE',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
