<?php

//@formatter:off

return [
    'name'        => 'TYPE_PIECE_JOINTE_STATUT_HDFK',
    'table'       => 'TYPE_PIECE_JOINTE_STATUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
