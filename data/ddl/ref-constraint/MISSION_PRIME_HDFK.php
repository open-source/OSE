<?php

//@formatter:off

return [
    'name'        => 'MISSION_PRIME_HDFK',
    'table'       => 'MISSION_PRIME',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
