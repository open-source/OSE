<?php

//@formatter:off

return [
    'name'        => 'MODIFICATION_SERVICE_DU_HCFK',
    'table'       => 'MODIFICATION_SERVICE_DU',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
