<?php

//@formatter:off

return [
    'name'        => 'VVHM_VALIDATION_FK',
    'table'       => 'VALIDATION_VOL_HORAIRE_MISS',
    'rtable'      => 'VALIDATION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
