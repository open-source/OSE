<?php

//@formatter:off

return [
    'name'        => 'FONCTION_REFERENTIEL_SFK',
    'table'       => 'FONCTION_REFERENTIEL',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
