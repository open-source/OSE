<?php

//@formatter:off

return [
    'name'        => 'FORMULE_R_VH_SERVCIE_FK',
    'table'       => 'FORMULE_RESULTAT_VOLUME_HORAIRE',
    'rtable'      => 'SERVICE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SERVICE_ID' => 'ID',
    ],
];

//@formatter:on
