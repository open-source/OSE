<?php

//@formatter:off

return [
    'name'        => 'FORMULE_R_INT_TYPE_INTERVENANT_FK',
    'table'       => 'FORMULE_RESULTAT_INTERVENANT',
    'rtable'      => 'TYPE_INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
