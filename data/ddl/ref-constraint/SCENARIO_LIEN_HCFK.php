<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_LIEN_HCFK',
    'table'       => 'SCENARIO_LIEN',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
