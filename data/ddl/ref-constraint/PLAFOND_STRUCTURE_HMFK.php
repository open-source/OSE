<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STRUCTURE_HMFK',
    'table'       => 'PLAFOND_STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
