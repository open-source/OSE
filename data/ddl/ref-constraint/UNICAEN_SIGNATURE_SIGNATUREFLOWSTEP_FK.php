<?php

//@formatter:off

return [
    'name'        => 'UNICAEN_SIGNATURE_SIGNATUREFLOWSTEP_FK',
    'table'       => 'UNICAEN_SIGNATURE_SIGNATUREFLOWSTEP',
    'rtable'      => 'UNICAEN_SIGNATURE_SIGNATUREFLOW',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SIGNATUREFLOW_ID' => 'ID',
    ],
];

//@formatter:on
