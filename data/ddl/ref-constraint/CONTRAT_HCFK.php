<?php

//@formatter:off

return [
    'name'        => 'CONTRAT_HCFK',
    'table'       => 'CONTRAT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
