<?php

//@formatter:off

return [
    'name'        => 'CAMPAGNE_SAISIE_ANNEE_FK',
    'table'       => 'CAMPAGNE_SAISIE',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
