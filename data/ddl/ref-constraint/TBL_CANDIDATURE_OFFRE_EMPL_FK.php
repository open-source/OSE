<?php

//@formatter:off

return [
    'name'        => 'TBL_CANDIDATURE_OFFRE_EMPL_FK',
    'table'       => 'TBL_CANDIDATURE',
    'rtable'      => 'OFFRE_EMPLOI',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'OFFRE_EMPLOI_ID' => 'ID',
    ],
];

//@formatter:on
