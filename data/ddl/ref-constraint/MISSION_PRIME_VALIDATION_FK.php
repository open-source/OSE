<?php

//@formatter:off

return [
    'name'        => 'MISSION_PRIME_VALIDATION_FK',
    'table'       => 'MISSION_PRIME',
    'rtable'      => 'VALIDATION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
