<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_R_HDFK',
    'table'       => 'AFFECTATION_RECHERCHE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
