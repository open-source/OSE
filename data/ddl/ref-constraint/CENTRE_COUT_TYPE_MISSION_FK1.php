<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_TYPE_MISSION_FK1',
    'table'       => 'CENTRE_COUT_TYPE_MISSION',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
