<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STRUCTURE_STRUCTURE_FK',
    'table'       => 'PLAFOND_STRUCTURE',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
