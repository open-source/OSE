<?php

//@formatter:off

return [
    'name'        => 'TBL_CONTRAT_TYPE_SERVICE_FK',
    'table'       => 'TBL_CONTRAT',
    'rtable'      => 'TYPE_SERVICE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_SERVICE_ID' => 'ID',
    ],
];

//@formatter:on
