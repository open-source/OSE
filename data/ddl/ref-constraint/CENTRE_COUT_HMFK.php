<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_HMFK',
    'table'       => 'CENTRE_COUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
