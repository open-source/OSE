<?php

//@formatter:off

return [
    'name'        => 'CONTRAT_HMFK',
    'table'       => 'CONTRAT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
