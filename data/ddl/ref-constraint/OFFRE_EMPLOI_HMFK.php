<?php

//@formatter:off

return [
    'name'        => 'OFFRE_EMPLOI_HMFK',
    'table'       => 'OFFRE_EMPLOI',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
