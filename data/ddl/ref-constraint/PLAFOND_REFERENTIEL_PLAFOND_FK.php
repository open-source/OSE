<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_REFERENTIEL_PLAFOND_FK',
    'table'       => 'PLAFOND_REFERENTIEL',
    'rtable'      => 'PLAFOND',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ID' => 'ID',
    ],
];

//@formatter:on
