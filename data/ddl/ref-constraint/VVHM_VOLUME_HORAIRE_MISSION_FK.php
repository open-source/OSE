<?php

//@formatter:off

return [
    'name'        => 'VVHM_VOLUME_HORAIRE_MISSION_FK',
    'table'       => 'VALIDATION_VOL_HORAIRE_MISS',
    'rtable'      => 'VOLUME_HORAIRE_MISSION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'VOLUME_HORAIRE_MISSION_ID' => 'ID',
    ],
];

//@formatter:on
