<?php

//@formatter:off

return [
    'name'        => 'MEP_DOMAINE_FONCTIONNEL_FK',
    'table'       => 'MISE_EN_PAIEMENT',
    'rtable'      => 'DOMAINE_FONCTIONNEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'DOMAINE_FONCTIONNEL_ID' => 'ID',
    ],
];

//@formatter:on
