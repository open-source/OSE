<?php

//@formatter:off

return [
    'name'        => 'TBL_AGR_TYPE_AGREMENT_FK',
    'table'       => 'TBL_AGREMENT',
    'rtable'      => 'TYPE_AGREMENT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_AGREMENT_ID' => 'ID',
    ],
];

//@formatter:on
