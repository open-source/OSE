<?php

//@formatter:off

return [
    'name'        => 'EFFECTIFS_ETAPE_ETAPE_FK',
    'table'       => 'EFFECTIFS_ETAPE',
    'rtable'      => 'ETAPE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ETAPE_ID' => 'ID',
    ],
];

//@formatter:on
