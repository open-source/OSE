<?php

//@formatter:off

return [
    'name'        => 'PERIODE_HMFK',
    'table'       => 'PERIODE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
