<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_REF_ETAT_PREVU_FK',
    'table'       => 'PLAFOND_REFERENTIEL',
    'rtable'      => 'PLAFOND_ETAT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ETAT_PREVU_ID' => 'ID',
    ],
];

//@formatter:on
