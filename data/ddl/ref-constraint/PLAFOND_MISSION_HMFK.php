<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_MISSION_HMFK',
    'table'       => 'PLAFOND_MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
