<?php

//@formatter:off

return [
    'name'        => 'MISE_EN_PAIEMENT_SERVICE_REFERENTIEL_FK',
    'table'       => 'MISE_EN_PAIEMENT',
    'rtable'      => 'SERVICE_REFERENTIEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SERVICE_REFERENTIEL_ID' => 'ID',
    ],
];

//@formatter:on
