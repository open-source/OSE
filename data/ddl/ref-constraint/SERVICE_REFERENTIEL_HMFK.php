<?php

//@formatter:off

return [
    'name'        => 'SERVICE_REFERENTIEL_HMFK',
    'table'       => 'SERVICE_REFERENTIEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
