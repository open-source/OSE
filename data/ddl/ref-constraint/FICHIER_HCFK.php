<?php

//@formatter:off

return [
    'name'        => 'FICHIER_HCFK',
    'table'       => 'FICHIER',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
