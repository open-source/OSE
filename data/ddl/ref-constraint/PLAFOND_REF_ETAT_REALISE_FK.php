<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_REF_ETAT_REALISE_FK',
    'table'       => 'PLAFOND_REFERENTIEL',
    'rtable'      => 'PLAFOND_ETAT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ETAT_REALISE_ID' => 'ID',
    ],
];

//@formatter:on
