<?php

//@formatter:off

return [
    'name'        => 'MOTIF_NON_PAIEMENT_HCFK',
    'table'       => 'MOTIF_NON_PAIEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
