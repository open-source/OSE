<?php

//@formatter:off

return [
    'name'        => 'FTI_FORMULE_FK',
    'table'       => 'FORMULE_TEST_INTERVENANT',
    'rtable'      => 'FORMULE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'FORMULE_ID' => 'ID',
    ],
];

//@formatter:on
