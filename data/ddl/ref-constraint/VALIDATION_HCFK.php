<?php

//@formatter:off

return [
    'name'        => 'VALIDATION_HCFK',
    'table'       => 'VALIDATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
