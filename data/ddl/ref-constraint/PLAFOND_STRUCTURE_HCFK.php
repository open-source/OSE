<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STRUCTURE_HCFK',
    'table'       => 'PLAFOND_STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
