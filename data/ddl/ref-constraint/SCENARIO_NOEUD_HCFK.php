<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_NOEUD_HCFK',
    'table'       => 'SCENARIO_NOEUD',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
