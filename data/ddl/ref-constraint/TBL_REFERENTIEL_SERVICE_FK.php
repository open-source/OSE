<?php

//@formatter:off

return [
    'name'        => 'TBL_REFERENTIEL_SERVICE_FK',
    'table'       => 'TBL_REFERENTIEL',
    'rtable'      => 'SERVICE_REFERENTIEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SERVICE_REFERENTIEL_ID' => 'ID',
    ],
];

//@formatter:on
