<?php

//@formatter:off

return [
    'name'        => 'DISCIPLINE_HDFK',
    'table'       => 'DISCIPLINE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
