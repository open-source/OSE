<?php

//@formatter:off

return [
    'name'        => 'TBL_PLAFOND_MISSION_PLAFOND_FK',
    'table'       => 'TBL_PLAFOND_MISSION',
    'rtable'      => 'PLAFOND',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ID' => 'ID',
    ],
];

//@formatter:on
