<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_EMPLOYEUR_FK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'EMPLOYEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'EMPLOYEUR_ID' => 'ID',
    ],
];

//@formatter:on
