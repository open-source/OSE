<?php

//@formatter:off

return [
    'name'        => 'MODULATEUR_TYPE_MODULATEUR_FK',
    'table'       => 'MODULATEUR',
    'rtable'      => 'TYPE_MODULATEUR',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_MODULATEUR_ID' => 'ID',
    ],
];

//@formatter:on
