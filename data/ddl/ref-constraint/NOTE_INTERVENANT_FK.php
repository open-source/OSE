<?php

//@formatter:off

return [
    'name'        => 'NOTE_INTERVENANT_FK',
    'table'       => 'NOTE',
    'rtable'      => 'INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
