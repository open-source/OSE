<?php

//@formatter:off

return [
    'name'        => 'TBL_CSD_SCENARIO_FK',
    'table'       => 'TBL_CHARGENS_SEUILS_DEF',
    'rtable'      => 'SCENARIO',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SCENARIO_ID' => 'ID',
    ],
];

//@formatter:on
