<?php

//@formatter:off

return [
    'name'        => 'TBL_PLA_INT_INT_FK',
    'table'       => 'TBL_PLAFOND_INTERVENANT',
    'rtable'      => 'INTERVENANT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
