<?php

//@formatter:off

return [
    'name'        => 'CANDIDATURE_VALIDATION_FK',
    'table'       => 'CANDIDATURE',
    'rtable'      => 'VALIDATION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
