<?php

//@formatter:off

return [
    'name'        => 'TI_TYPE_INTERVENTION_FK',
    'table'       => 'TYPE_INTERVENTION',
    'rtable'      => 'TYPE_INTERVENTION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENTION_MAQUETTE_ID' => 'ID',
    ],
];

//@formatter:on
