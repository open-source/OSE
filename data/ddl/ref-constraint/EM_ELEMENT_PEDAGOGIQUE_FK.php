<?php

//@formatter:off

return [
    'name'        => 'EM_ELEMENT_PEDAGOGIQUE_FK',
    'table'       => 'ELEMENT_MODULATEUR',
    'rtable'      => 'ELEMENT_PEDAGOGIQUE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'ELEMENT_ID' => 'ID',
    ],
];

//@formatter:on
