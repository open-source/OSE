<?php

//@formatter:off

return [
    'name'        => 'CONTRAT_CONTRAT_FK',
    'table'       => 'CONTRAT',
    'rtable'      => 'CONTRAT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'CONTRAT_ID' => 'ID',
    ],
];

//@formatter:on
