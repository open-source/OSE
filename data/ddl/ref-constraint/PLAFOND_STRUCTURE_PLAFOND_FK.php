<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STRUCTURE_PLAFOND_FK',
    'table'       => 'PLAFOND_STRUCTURE',
    'rtable'      => 'PLAFOND',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ID' => 'ID',
    ],
];

//@formatter:on
