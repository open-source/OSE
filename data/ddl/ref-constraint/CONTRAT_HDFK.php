<?php

//@formatter:off

return [
    'name'        => 'CONTRAT_HDFK',
    'table'       => 'CONTRAT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
