<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_STRUCTURE_HCFK',
    'table'       => 'CENTRE_COUT_STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
