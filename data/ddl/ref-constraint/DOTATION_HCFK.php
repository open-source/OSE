<?php

//@formatter:off

return [
    'name'        => 'DOTATION_HCFK',
    'table'       => 'DOTATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
