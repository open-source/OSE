<?php

//@formatter:off

return [
    'name'        => 'WE_PREC_WE_FK',
    'table'       => 'WF_ETAPE_DEP',
    'rtable'      => 'WF_ETAPE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'ETAPE_PREC_ID' => 'ID',
    ],
];

//@formatter:on
