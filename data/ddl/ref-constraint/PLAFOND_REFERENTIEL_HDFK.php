<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_REFERENTIEL_HDFK',
    'table'       => 'PLAFOND_REFERENTIEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
