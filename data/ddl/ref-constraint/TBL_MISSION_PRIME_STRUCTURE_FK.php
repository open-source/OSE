<?php

//@formatter:off

return [
    'name'        => 'TBL_MISSION_PRIME_STRUCTURE_FK',
    'table'       => 'TBL_MISSION_PRIME',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
