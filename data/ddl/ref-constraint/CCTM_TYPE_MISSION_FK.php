<?php

//@formatter:off

return [
    'name'        => 'CCTM_TYPE_MISSION_FK',
    'table'       => 'CENTRE_COUT_TYPE_MISSION',
    'rtable'      => 'TYPE_MISSION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_MISSION_ID' => 'ID',
    ],
];

//@formatter:on
