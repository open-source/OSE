<?php

//@formatter:off

return [
    'name'        => 'VH_ENS_HMFK',
    'table'       => 'VOLUME_HORAIRE_ENS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
