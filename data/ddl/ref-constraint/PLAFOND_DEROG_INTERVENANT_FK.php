<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_DEROG_INTERVENANT_FK',
    'table'       => 'PLAFOND_DEROGATION',
    'rtable'      => 'INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
