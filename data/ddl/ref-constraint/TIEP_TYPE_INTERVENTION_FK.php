<?php

//@formatter:off

return [
    'name'        => 'TIEP_TYPE_INTERVENTION_FK',
    'table'       => 'TYPE_INTERVENTION_EP',
    'rtable'      => 'TYPE_INTERVENTION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENTION_ID' => 'ID',
    ],
];

//@formatter:on
