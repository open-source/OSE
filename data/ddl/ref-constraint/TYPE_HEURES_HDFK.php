<?php

//@formatter:off

return [
    'name'        => 'TYPE_HEURES_HDFK',
    'table'       => 'TYPE_HEURES',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
