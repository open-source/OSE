<?php

//@formatter:off

return [
    'name'        => 'VOLUME_HORAIRE_MISSION_HMFK',
    'table'       => 'VOLUME_HORAIRE_MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
