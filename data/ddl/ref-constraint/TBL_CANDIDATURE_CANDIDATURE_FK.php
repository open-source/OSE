<?php

//@formatter:off

return [
    'name'        => 'TBL_CANDIDATURE_CANDIDATURE_FK',
    'table'       => 'TBL_CANDIDATURE',
    'rtable'      => 'CANDIDATURE',
    'delete_rule' => 'SET NULL',
    'index'       => NULL,
    'columns'     => [
        'CANDIDATURE_ID' => 'ID',
    ],
];

//@formatter:on
