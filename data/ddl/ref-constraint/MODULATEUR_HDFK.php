<?php

//@formatter:off

return [
    'name'        => 'MODULATEUR_HDFK',
    'table'       => 'MODULATEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
