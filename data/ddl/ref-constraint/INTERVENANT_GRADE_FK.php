<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_GRADE_FK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'GRADE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'GRADE_ID' => 'ID',
    ],
];

//@formatter:on
