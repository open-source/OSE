<?php

//@formatter:off

return [
    'name'        => 'OFFRE_EMPLOI_HCFK',
    'table'       => 'OFFRE_EMPLOI',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
