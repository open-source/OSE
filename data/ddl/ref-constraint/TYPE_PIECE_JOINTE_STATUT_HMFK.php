<?php

//@formatter:off

return [
    'name'        => 'TYPE_PIECE_JOINTE_STATUT_HMFK',
    'table'       => 'TYPE_PIECE_JOINTE_STATUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
