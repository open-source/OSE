<?php

//@formatter:off

return [
    'name'        => 'MODIFICATION_SERVICE_DU_HMFK',
    'table'       => 'MODIFICATION_SERVICE_DU',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
