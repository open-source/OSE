<?php

//@formatter:off

return [
    'name'        => 'TBL_CONTRAT_TYPE_CONTRAT_FK',
    'table'       => 'TBL_CONTRAT',
    'rtable'      => 'TYPE_CONTRAT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_CONTRAT_ID' => 'ID',
    ],
];

//@formatter:on
