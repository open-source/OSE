<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_DEROGATION_PLAFOND_FK',
    'table'       => 'PLAFOND_DEROGATION',
    'rtable'      => 'PLAFOND',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ID' => 'ID',
    ],
];

//@formatter:on
