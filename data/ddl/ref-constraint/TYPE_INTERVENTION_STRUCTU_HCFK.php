<?php

//@formatter:off

return [
    'name'        => 'TYPE_INTERVENTION_STRUCTU_HCFK',
    'table'       => 'TYPE_INTERVENTION_STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
