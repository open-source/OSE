<?php

//@formatter:off

return [
    'name'        => 'VVH_VALIDATION_FK',
    'table'       => 'VALIDATION_VOL_HORAIRE',
    'rtable'      => 'VALIDATION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
