<?php

//@formatter:off

return [
    'name'        => 'UNICAEN_SIGNATURE_PROCESS_STEP_PFK',
    'table'       => 'UNICAEN_SIGNATURE_PROCESS_STEP',
    'rtable'      => 'UNICAEN_SIGNATURE_PROCESS',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PROCESS_ID' => 'ID',
    ],
];

//@formatter:on
