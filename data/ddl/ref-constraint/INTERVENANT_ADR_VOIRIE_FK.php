<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_ADR_VOIRIE_FK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'VOIRIE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ADRESSE_VOIRIE_ID' => 'ID',
    ],
];

//@formatter:on
