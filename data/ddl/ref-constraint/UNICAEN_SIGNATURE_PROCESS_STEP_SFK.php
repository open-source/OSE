<?php

//@formatter:off

return [
    'name'        => 'UNICAEN_SIGNATURE_PROCESS_STEP_SFK',
    'table'       => 'UNICAEN_SIGNATURE_PROCESS_STEP',
    'rtable'      => 'UNICAEN_SIGNATURE_SIGNATURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SIGNATURE_ID' => 'ID',
    ],
];

//@formatter:on
