<?php

//@formatter:off

return [
    'name'        => 'NOTE_HCFK',
    'table'       => 'NOTE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
