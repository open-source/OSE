<?php

//@formatter:off

return [
    'name'        => 'TYPE_INTERVENTION_EP_HMFK',
    'table'       => 'TYPE_INTERVENTION_EP',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
