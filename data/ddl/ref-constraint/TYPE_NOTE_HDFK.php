<?php

//@formatter:off

return [
    'name'        => 'TYPE_NOTE_HDFK',
    'table'       => 'TYPE_NOTE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
