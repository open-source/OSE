<?php

//@formatter:off

return [
    'name'        => 'SEUIL_CH_UTILISATEUR_HCFK',
    'table'       => 'SEUIL_CHARGE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
