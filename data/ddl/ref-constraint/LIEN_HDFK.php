<?php

//@formatter:off

return [
    'name'        => 'LIEN_HDFK',
    'table'       => 'LIEN',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
