<?php

//@formatter:off

return [
    'name'        => 'ETAPE_STRUCTURE_FK',
    'table'       => 'ETAPE',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
