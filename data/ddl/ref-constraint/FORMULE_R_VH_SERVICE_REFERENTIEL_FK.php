<?php

//@formatter:off

return [
    'name'        => 'FORMULE_R_VH_SERVICE_REFERENTIEL_FK',
    'table'       => 'FORMULE_RESULTAT_VOLUME_HORAIRE',
    'rtable'      => 'SERVICE_REFERENTIEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SERVICE_REFERENTIEL_ID' => 'ID',
    ],
];

//@formatter:on
