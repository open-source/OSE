<?php

//@formatter:off

return [
    'name'        => 'AGREMENT_TYPE_AGREMENT_FK',
    'table'       => 'AGREMENT',
    'rtable'      => 'TYPE_AGREMENT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_AGREMENT_ID' => 'ID',
    ],
];

//@formatter:on
