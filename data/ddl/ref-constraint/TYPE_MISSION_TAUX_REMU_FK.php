<?php

//@formatter:off

return [
    'name'        => 'TYPE_MISSION_TAUX_REMU_FK',
    'table'       => 'TYPE_MISSION',
    'rtable'      => 'TAUX_REMU',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TAUX_REMU_ID' => 'ID',
    ],
];

//@formatter:on
