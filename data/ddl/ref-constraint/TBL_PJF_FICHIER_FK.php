<?php

//@formatter:off

return [
    'name'        => 'TBL_PJF_FICHIER_FK',
    'table'       => 'TBL_PIECE_JOINTE_FOURNIE',
    'rtable'      => 'FICHIER',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'FICHIER_ID' => 'ID',
    ],
];

//@formatter:on
