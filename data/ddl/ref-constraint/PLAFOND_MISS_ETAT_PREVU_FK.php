<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_MISS_ETAT_PREVU_FK',
    'table'       => 'PLAFOND_MISSION',
    'rtable'      => 'PLAFOND_ETAT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ETAT_PREVU_ID' => 'ID',
    ],
];

//@formatter:on
