<?php

//@formatter:off

return [
    'name'        => 'TPJS_TYPE_PIECE_JOINTE_FK',
    'table'       => 'TYPE_PIECE_JOINTE_STATUT',
    'rtable'      => 'TYPE_PIECE_JOINTE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_PIECE_JOINTE_ID' => 'ID',
    ],
];

//@formatter:on
