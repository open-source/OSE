<?php

//@formatter:off

return [
    'name'        => 'STRUCTURE_STRUCTURE_FK',
    'table'       => 'STRUCTURE',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => 'SET NULL',
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
