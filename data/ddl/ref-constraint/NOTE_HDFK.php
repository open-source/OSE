<?php

//@formatter:off

return [
    'name'        => 'NOTE_HDFK',
    'table'       => 'NOTE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
