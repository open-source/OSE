<?php

//@formatter:off

return [
    'name'        => 'STRUCTURE_ADR_PAYS_FK',
    'table'       => 'STRUCTURE',
    'rtable'      => 'PAYS',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ADRESSE_PAYS_ID' => 'ID',
    ],
];

//@formatter:on
