<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_STRUCTURE_SOURCE_FK',
    'table'       => 'CENTRE_COUT_STRUCTURE',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
