<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_SOURCE_FK',
    'table'       => 'CENTRE_COUT',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
