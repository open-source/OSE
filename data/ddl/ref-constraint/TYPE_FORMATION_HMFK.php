<?php

//@formatter:off

return [
    'name'        => 'TYPE_FORMATION_HMFK',
    'table'       => 'TYPE_FORMATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
