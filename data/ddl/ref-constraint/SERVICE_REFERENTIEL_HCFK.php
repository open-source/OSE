<?php

//@formatter:off

return [
    'name'        => 'SERVICE_REFERENTIEL_HCFK',
    'table'       => 'SERVICE_REFERENTIEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
