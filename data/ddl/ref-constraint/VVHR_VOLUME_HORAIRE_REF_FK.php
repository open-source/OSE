<?php

//@formatter:off

return [
    'name'        => 'VVHR_VOLUME_HORAIRE_REF_FK',
    'table'       => 'VALIDATION_VOL_HORAIRE_REF',
    'rtable'      => 'VOLUME_HORAIRE_REF',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'VOLUME_HORAIRE_REF_ID' => 'ID',
    ],
];

//@formatter:on
