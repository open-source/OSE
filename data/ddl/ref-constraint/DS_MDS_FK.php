<?php

//@formatter:off

return [
    'name'        => 'DS_MDS_FK',
    'table'       => 'MODIFICATION_SERVICE_DU',
    'rtable'      => 'MOTIF_MODIFICATION_SERVICE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'MOTIF_ID' => 'ID',
    ],
];

//@formatter:on
