export default {
    /* UI components */
    UHeures: 'Application/UI/UHeures',
    UInputFloat: 'Application/UI/UInputFloat',

    /* App common components */
    Utilisateur: 'Application/Utilisateur',

};