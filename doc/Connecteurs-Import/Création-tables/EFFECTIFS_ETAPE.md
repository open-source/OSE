# EFFECTIFS_ETAPE

Liste des effectifs par étapes

Colonnes nécessaires :

|Colonne    |Type    |Longueur|Nullable|Commentaire          |
|-----------|--------|--------|--------|---------------------|
|Z_ETAPE_ID |NUMBER  |        |Non     |==> ETAPE.SOURCE_CODE|
|FI         |NUMBER  |        |Non     |                     |
|FC         |NUMBER  |        |Non     |                     |
|FA         |NUMBER  |        |Non     |                     |
|Z_SOURCE_ID|NUMBER  |        |Non     |==> SOURCE.CODE      |
|SOURCE_CODE|VARCHAR2|100     |Non     |                     |
