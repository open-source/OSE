# DOMAINE_FONCTIONNEL

Liste des domaines fonctionnels

Colonnes nécessaires :

|Colonne    |Type    |Longueur|Nullable|Commentaire|
|-----------|--------|--------|--------|-----------|
|LIBELLE    |VARCHAR2|200     |Non     |           |
|Z_SOURCE_ID|NUMBER  |        |Non     |==> SOURCE.CODE|
|SOURCE_CODE|VARCHAR2|100     |Non     |           |


Exemple de requête :
[SRC_DOMAINE_FONCTIONNEL](../Sifac/SRC_DOMAINE_FONCTIONNEL.sql)