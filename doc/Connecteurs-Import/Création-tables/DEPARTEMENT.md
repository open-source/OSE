# DEPARTEMENT

Liste des départements

Colonnes nécessaires :

|Colonne    |Type    |Longueur|Nullable|Commentaire|
|-----------|--------|--------|--------|-----------|
|CODE       |VARCHAR2|5       |Non     |           |
|LIBELLE    |VARCHAR2|120     |Non     |           |
|Z_SOURCE_ID|NUMBER  |        |Non     |==> SOURCE.CODE|
|SOURCE_CODE|VARCHAR2|100     |Non     |           |


Exemple de requête :
[SRC_DEPARTEMENT](../Harpège/SRC_DEPARTEMENT.sql)