  CREATE OR REPLACE FORCE EDITIONABLE VIEW "GRHUM"."V_ULH_INDIVIDU_BANQUE" ("NO_DOSSIER_PERS", "BIC", "IBAN", "RIB_TITCO", "RIB_VALIDE", "IND_ACTIVITE", "D_CREATION", "D_MODIFICATION") AS 
  SELECT ind.NO_INDIVIDU, nvl(rib.BIC, bq.BIC), rib.IBAN, rib.RIB_TITCO, rib.RIB_VALIDE, ind.IND_ACTIVITE, rib.D_CREATION, rib.D_MODIFICATION
  FROM INDIVIDU_ULR ind
  JOIN FOURNIS_ULR fou USING (PERS_ID)
  JOIN RIBFOUR_ULR rib USING (FOU_ORDRE)
   --novembre 2018 : ajout jointure BANQUE pour recuperer tous les bic
  JOIN BANQUE bq USING (BANQ_ORDRE)
  WHERE RIB_VALIDE='O'
  -- ajout EC
  and ind.ind_activite is not null;
