# Documentation technique de OSE

## Ligne de commande

* [Utilisation de OSE en ligne de commande](ligne-de-commande.md)


## IMPORT : Synchronisation des données en provenance su SI

* [Connecteurs IMPORT](Connecteurs-Import/Connecteurs-IMPORT.md)


## EXPORT : Exploiter les données issues de OSE

* [Comment exploiter des données issues de OSE](export.md)
* [Modèle de données dédié à l'export](export-pilotage.md)
* [Créer ses propres scripts en s'appuyant sur l'application](scripts.md)

## Configuration

* [Plafonds](Plafonds/Plafonds.md)
* [Detecter si on est connecté depuis l'établissement ou bien depuis l'extérieur](detection-etablissement-ou-extérieur.md)
* [Stockage des fichiers](Stockage-fichiers.md)