<?php

namespace Plafond\Interfaces;

interface PlafondDataInterface
{
    public function getId(): ?int;
}