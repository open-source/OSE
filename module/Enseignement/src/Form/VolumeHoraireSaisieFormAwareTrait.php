<?php

namespace Enseignement\Form;

/**
 * Description of VolumeHoraireSaisieFormAwareTrait
 *
 * @author UnicaenCode
 */
trait VolumeHoraireSaisieFormAwareTrait
{
    protected ?VolumeHoraireSaisieForm $formVolumeHoraireSaisie = null;



    /**
     * @param VolumeHoraireSaisieForm $formVolumeHoraireSaisie
     *
     * @return self
     */
    public function setFormVolumeHoraireSaisie(?VolumeHoraireSaisieForm $formVolumeHoraireSaisie)
    {
        $this->formVolumeHoraireSaisie = $formVolumeHoraireSaisie;

        return $this;
    }



    public function getFormVolumeHoraireSaisie(): ?VolumeHoraireSaisieForm
    {
        if (!empty($this->formVolumeHoraireSaisie)) {
            return $this->formVolumeHoraireSaisie;
        }

        return \AppAdmin::container()->get('FormElementManager')->get(VolumeHoraireSaisieForm::class);
    }
}