<?php
/**
 * Generated by UnicaenCode
 * Commande : ose-code  module='Mission'
 */
return [
    'mission/offre-emploi/consulter'            => __DIR__ . '/view/mission/offre-emploi/consulter.phtml',
    'mission/offre-emploi/saisir'               => __DIR__ . '/view/mission/offre-emploi/saisir.phtml',
    'mission/offre-emploi/detail'               => __DIR__ . '/view/mission/offre-emploi/detail.phtml',
    'mission/offre-emploi/candidature'          => __DIR__ . '/view/mission/offre-emploi/candidature.phtml',
    'mission/offre-emploi/accepter-candidature' => __DIR__ . '/view/mission/offre-emploi/accepter-candidature.phtml',
    'mission/offre-emploi/index'                => __DIR__ . '/view/mission/offre-emploi/index.phtml',
    'mission/offre-emploi/public'               => __DIR__ . '/view/mission/offre-emploi/public.phtml',
    'mission/prime/saisie'                      => __DIR__ . '/view/mission/prime/saisie.phtml',
    'mission/prime/index'                       => __DIR__ . '/view/mission/prime/index.phtml',
    'mission/mission-type/saisir'               => __DIR__ . '/view/mission/mission-type/saisir.phtml',
    'mission/mission-type/centre-couts'         => __DIR__ . '/view/mission/mission-type/centre-couts.phtml',
    'mission/mission-type/visualiser'           => __DIR__ . '/view/mission/mission-type/visualiser.phtml',
    'mission/mission-type/index'                => __DIR__ . '/view/mission/mission-type/index.phtml',
    'mission/suivi/saisie'                      => __DIR__ . '/view/mission/suivi/saisie.phtml',
    'mission/suivi/index'                       => __DIR__ . '/view/mission/suivi/index.phtml',
    'mission/saisie/saisie'                     => __DIR__ . '/view/mission/saisie/saisie.phtml',
    'mission/saisie/index'                      => __DIR__ . '/view/mission/saisie/index.phtml',
];