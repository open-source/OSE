<?php

namespace Lieu;

return [
    'services' => [
        Service\AdresseNumeroComplService::class => Service\AdresseNumeroComplServiceFactory::class,
    ],
];