![Logo OSE](doc/logo.png)

Organisation des Services d'Enseignement

OSE permet :
- d'organiser les services d'enseignements
- de calculer le coût d'une offre de formation
- de suivre les emplois étudiants

Présentation rapide de l'application : http://ose.unicaen.fr/presentation

## Nouveautés

La [`Liste des changements`](CHANGELOG.md) vous permettra de savoir :
- si une version est sortie ;
- quelles en sont les nouveautés ;
- quelles sont les éventuels bugs corrigés ;
- s'il y a des notes de mise à jour à prendre en compte pour installer cette nouvelle version.

## Installation et mise à jour

Une [`procédure d'installation`](INSTALL.md) est à votre disposition pour mettre en place une nouvelle instance de OSE, que se soit en test ou en production.

La [`procédure de mise à jour`](UPDATE.md) vous indiquera comment mettre à niveau votre instance.

## Ressources vidéos

https://mediatheque-pedagogique.unicaen.fr/dsi/ose/

## Documentation technique

[`Documentation`](doc/doc.md).

## Nous contacter

Pour une première prise de contact, vous pouvez nous joindre au moyen de l'adresse
<deploiement-ose@unicaen.fr>